import sys, path
import mpld3
#import import_ipynb
import plots.avl_tree
from matplotlib import pyplot as plt
from shapely.geometry import Point, Polygon, LineString
from shapely.geometry.base import BaseGeometry, geos_geom_from_py
from shapely import affinity
import numpy
import numpy as np
import math
import mplcursors
from fractions import Fraction
import random
from copy import deepcopy
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from plotly.offline import plot
from bokeh.plotting import figure
from bokeh.io import output_notebook, show
from bokeh.layouts import gridplot, row, layout
from bokeh.models import Legend
from bokeh.models import Arrow, NormalHead, OpenHead, VeeHead
from bokeh.core.properties import Enum
from bokeh.layouts import column
from bokeh.models import Div, BoxAnnotation
from bokeh.models import ColumnDataSource, Label, LabelSet, Range1d
from bokeh.palettes import Dark2_5 as palette
from bokeh.palettes import Viridis256
from bokeh.models import CustomJS
import itertools

a = 0.407


class Polygon2(object):

    def __init__(self, shell=None, holes=None):
        # super().__init__(shell=None, holes=None)
        self.shell = shell
        self.x_values = [vertex[0] for vertex in shell]
        self.y_values = [vertex[1] for vertex in shell]
        self.min_x, self.max_x = self.min_x_max_x(shell)
        self.min_y, self.max_y = self.min_y_max_y(shell)
        self.height = self.height(shell)
        self.width = self.width(shell)
        self.spine = self.spine(self.shell)
        self.left, self.right = self.splitter()
        self.slope = self.slope(self.spine)

    def min_x_max_x(self, shell):
        x_values = [point[0] for point in shell]
        return (min(x_values), max(x_values))

    def min_y_max_y(self, shell):
        y_values = [point[1] for point in shell]
        return (min(y_values), max(y_values))

    def height(self, coord):
        height = self.max_y - self.min_y
        return height

    def width(self, coord):
        width = self.max_x - self.min_x
        return width

    def translation(self, x, y):
        self.shell = [(point[0] + x, point[1] + y) for point in self.shell]
        self.spine = ((self.spine[0][0] + x, self.spine[0][1] + y), (self.spine[1][0] + x, self.spine[1][1] + y))
        self.left = [(point[0] + x, point[1] + y) for point in self.left]
        self.right = [(point[0] + x, point[1] + y) for point in self.right]
        self.min_x = self.min_x + x
        self.max_x = self.max_x + x
        self.min_y = self.min_y + y
        self.max_y = self.max_y + y
        self.x_values = [vertex + x for vertex in self.x_values]
        self.y_values = [vertex + y for vertex in self.y_values]
        return self.shell

    def spine(self, shell):
        y_sorted_p_p = sorted(shell, key=lambda k: k[
            1])  # Parser schreibt oder davon ausgeht das konvexe Polygone übergeben werden
        if y_sorted_p_p[0][1] == y_sorted_p_p[1][1]:
            if y_sorted_p_p[0][0] < y_sorted_p_p[1][0]:
                spine_bottom = y_sorted_p_p[0]
            else:
                spine_bottom = y_sorted_p_p[1]
        else:
            spine_bottom = y_sorted_p_p[0]
        if y_sorted_p_p[-2][1] == y_sorted_p_p[-1][1]:
            if y_sorted_p_p[-2][0] < y_sorted_p_p[-1][0]:
                spine_top = y_sorted_p_p[-1]
            else:
                spine_top = y_sorted_p_p[-2]
        else:
            spine_top = y_sorted_p_p[-1]
        return (spine_bottom, spine_top)

    def slope(self, spine):
        if spine[0][0] == spine[1][0]:  # Sonderfall für eine senkrechte
            slope = math.inf
        else:
            slope = (spine[1][1] - spine[0][1]) / (spine[1][0] - spine[0][
                0])  # m = y2-y1/x2-x1 Formel für die Geradensteigung mithilfe aus zwei verschiedenen Punkten der Geraden
        return slope

    def plot_polygon(self, title="", render=True):
        """Plotting a Polygon with bokeh"""
        height = (int(self.height * 10)) / 10
        if self.slope == math.inf:
            slope = math.inf
        else:
            slope = (int(self.slope * 10)) / 10
        x_data = self.x_values
        y_data = self.y_values
        TOOLTIPS = [("index", "$index"), ("(x,y)", "($x, $y)"), ]
        if title == "":
            title = 'height:{}  slope:{}'.format(height, slope)
        else:
            title = '{} height:{}  slope:{}'.format(title, height, slope)
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', tooltips=TOOLTIPS, )
        fig.line(x_data, y_data, legend_label="Shell", line_width=2, muted_alpha=0.2)
        fig.circle(x_data, y_data, legend_label="Shell", line_width=2, muted_alpha=0.2, size=8)
        spine_x_values = [x[0] for x in self.spine]
        spine_y_values = [x[1] for x in self.spine]
        fig.line(spine_x_values, spine_y_values, line_color="red", legend_label="Spine", line_width=2, muted_alpha=0.2)
        fig.legend.click_policy = "mute"
        if render:
            return show(fig)
        else:
            return fig

    def splitter(self):
        left = []
        right = []
        spine_bottom = self.spine[0]
        spine_top = self.spine[1]
        help_array = self.shell.copy()
        spine_bottom_found = False
        spine_top_found = False
        spine_bottom_not_horizontal = False
        while spine_bottom_found == False:  # Phase 1 der Funktion sie sucht den bottom spine, dabei werden die Ecken die nicht der Spine Bottom sind ans Ende der Liste geschoben
            if help_array[0] == spine_bottom:
                if spine_bottom[1] != help_array[-1][
                    1]:  # checkt ob Spine bottom ein horizontalen Nachbar hat falls ja wird ein Flag gesetzt damit Spine bottomt später nicht in die Rechte mitgenommen wird
                    spine_bottom_not_horizontal = True
                spine_bottom_found = True
                left.append(help_array.pop(0))
            else:
                help_array.append(help_array.pop(0))
        while spine_top_found == False:  # iterieren die Liste erneut durch bis wir spine top finden, falls spine top gefunden wurde wird auch geschaut ob spine top ein horizontalen linken
            if help_array[0] == spine_top:  # Nachbar hat falls ja wird spine top nicht in die Linke Liste miteingefügt
                spine_top_found = True
                if spine_top[1] != left[-1][1]:
                    left.append(spine_top)
            else:
                left.append(help_array.pop(0))
        right = help_array.copy()
        if spine_bottom_not_horizontal:
            right.append(spine_bottom)
        return (left, right)


class HighClass(object):
    def __init__(self, i, alpha, h_max, w_max):
        self.i = i
        self.alpha = alpha
        self.h_max = h_max
        self.w_max = w_max
        self.min_border = alpha ** (i + 1) * h_max
        self.max_border = alpha ** (i) * h_max
        self.polygons = []
        self.spine_ordered_polygons = []

    def spine_order_polygons(self):
        spine_ordered_polygons = sorted(self.polygons, key=lambda polygon: polygon.slope)
        self.spine_ordered_polygons = spine_ordered_polygons
        return spine_ordered_polygons

    def plot_hc(self, ordered=False,stretch=False, render=True, plot_width=250, plot_height=250):
        plots = []
        if ordered:
            polygon_list = self.spine_ordered_polygons
        else:
            polygon_list = self.polygons
        for counter, polygon in enumerate(polygon_list):
            title = "Polygon {}".format(counter)
            plots.append(polygon.plot_polygon(title=title, render=False))
        if stretch:
            grid = gridplot(plots, ncols=4, sizing_mode='stretch_both')
        else:
            grid = gridplot(plots, ncols=4, plot_width=plot_width, plot_height=plot_height)
        if render:
            return show(grid)
        else:
            return grid


def height_classes(polygon_list):
    ordered_polygons = sorted(polygon_list, key=lambda polygon: polygon.height, reverse=True)
    h_max = ordered_polygons[0].height
    # max([polygon.height for polygon in polygon_list])
    w_max = max([polygon.width for polygon in polygon_list])
    alpha = 0.407
    i = 0
    # h_i = height_max
    height_classes = []
    response = []
    polygon_count = len(ordered_polygons)
    while polygon_count > 0:
        hc = HighClass(i, alpha, h_max, w_max)
        # polygon_count= len(ordered_polygons)
        while polygon_count > 0 and hc.min_border < ordered_polygons[0].height and ordered_polygons[
            0].height <= hc.max_border:
            # print (alpha**(i+1)*h_max, ordered_polygons[0].height,alpha**(i)*h_max) # print
            hc.polygons.append(ordered_polygons.pop(0))
            polygon_count -= 1
        hc.spine_order_polygons()
        if len(hc.polygons) > 0:
            height_classes.append(hc)  # will man höhen Klassen ohne Polygone drinne ?
            # response.append([polygon.height for polygon in hc.polygons]) # print
        i += 1
    # print(Hresponse)
    # print([y.polygons for y in height_classes])
    return height_classes


def create_multiple_convex_polygons(number, max_ngon):
    polygon_list = []
    for count in range(0, number):
        polygon_list.append(create_convex_polygon(max_ngon))
    return polygon_list


def create_convex_polygon(
        max_ngon):  # die erste Koordinate der konvexen Polygone wird dupliziert zum schließen des Polygons
    convex = False
    ngon = numpy.random.randint(3, max_ngon + 1)
    # ries=0
    max_rand = 1000

    # tries=tries+1
    polygon_points = []
    while len(polygon_points) < ngon:

        x = numpy.random.randint(1, max_rand)
        y = numpy.random.randint(1, max_rand)
        #             x = numpy.random.random()
        #             y = numpy.random.random()
        while (x, y) in polygon_points:
            #             x = numpy.random.random()
            #             y = numpy.random.random()
            x = numpy.random.randint(1, max_rand)
            y = numpy.random.randint(1, max_rand)
        polygon_points.append((x, y))

    polygon = Polygon(polygon_points)
    polygon_parent_class = polygon.convex_hull
    polygon2 = Polygon2(list(polygon_parent_class.exterior.coords))
    # randomint = numpy.random.randint(minvert,maxvert)
    # response= numpy.random.rand(randomint,2)
    return polygon2


def plot_polygons(polygon_list, render=True, stretch=False,plot_width=250, plot_height=250):
    plots = []
    for counter, polygon in enumerate(polygon_list):
        title = "Polygon {}".format(counter)
        plots.append(polygon.plot_polygon(title=title, render=False))
    if stretch:
        grid = gridplot(plots, ncols=4, sizing_mode='stretch_both')
    else:
        grid = gridplot(plots, ncols=4, plot_width=plot_width, plot_height=plot_height)
    if render:
        return show(grid)
    else:
        return grid


def pack_polygons(polygons):
    list_hc = height_classes(polygons)
    list_containers = building_containers(list_hc)
    list_mini_containers = pack_mini_containers(list_containers)
    end_container = End_Container(list_mini_containers, True)
    return end_container


class Container(object):
    def __init__(self, hc):
        self.hc = hc
        self.hc_copy = deepcopy(hc)
        self.y_boundary = hc.max_border
        self.x_boundary = 0
        self.sigma = []
        self.Tree = plots.avl_tree.AVLTree()
        self.root = None
        self.box_boundarys_x_values = []
        self.plot_steps = []

    def plot_container(self, sigma=None, r_l_distances=None, l_r_distances=None, min_distance=None, render=True,
                       title="", background_c_list=None):
        legend_polygons = []
        legend_spine = []
        legend_lr = []
        legend_rl = []
        if sigma == None:
            sigma = self.sigma
        TOOLTIPS = [("index", "$index"), ("(x,y)", "($x, $y)"), ]
        #         if title:
        #             title=title
        #         else:
        #             title=""
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', tooltips=TOOLTIPS, toolbar_location="below")
        for counter, polygon in enumerate(sigma):
            x_values = []
            y_values = []
            for x, y in polygon.shell:
                x_values.append(x)
                y_values.append(y)
            poly_fig = fig.line(x_values, y_values, line_width=2, muted_alpha=0.2)
            circle_fig = fig.circle(x_values, y_values, line_width=2, muted_alpha=0.2)
            legend_label = "Polygon{}".format(counter)
            legend_polygons.append((legend_label, [poly_fig, circle_fig]))
            x_spine = [x[0] for x in polygon.spine]
            y_spine = [y[1] for y in polygon.spine]

            spine_fig = fig.line(x_spine, y_spine, line_color="red", line_width=2, muted_alpha=0.2)
            legend_spine.append(spine_fig)
        # mini-Container helper
        if self.box_boundarys_x_values != []:
            # print("self.box_boundarys_x_values",self.box_boundarys_x_values)
            for counter, boundary in enumerate(self.box_boundarys_x_values[0:-1]):
                next_boundary = self.box_boundarys_x_values[counter + 1]
                if background_c_list != None:
                    color_box = BoxAnnotation(bottom=0, top=self.y_boundary, left=self.box_boundarys_x_values[counter],
                                              right=next_boundary, fill_color=background_c_list[counter],
                                              fill_alpha=0.1)
                    fig.add_layout(color_box)
                fig.line([next_boundary, next_boundary], [0, self.y_boundary], line_dash="dotted")
            fig.title.text = 'hc_{} Container -> Mini Containers'.format(self.hc.i)
        if title == "":
            fig.title.text = 'hc_{} Container'.format(self.hc.i)
        if r_l_distances != None:
            for tuple in r_l_distances:
                x = tuple[0][0]
                corresponding_point_x = x - tuple[1]
                y = tuple[0][1]
                text_point_x, text_point_y = (x - tuple[1] / 2, y)
                distance = int(tuple[1])
                color = "blue"
                if distance == int(min_distance):
                    line_dash = "solid"
                else:
                    line_dash = "dotted"
                fig_rl = fig.line([tuple[0][0], corresponding_point_x], [y, y], line_color=color, line_width=2,
                                  muted_alpha=0.2, line_dash=line_dash)
                source = ColumnDataSource(data=dict(x=[tuple[0][0] - (tuple[1] / 2)], y=[y], names=[distance]))
                labels = LabelSet(x='x', y='y', text='names', level='glyph', x_offset=0, y_offset=0, source=source,
                                  render_mode='canvas')
                fig.add_layout(labels)
                # fig_triangle_rl=fig.triangle([tuple[0][0],corresponding_point_x],[y,y],line_color=color,line_width=2, muted_alpha=0.2,angle=-90,angle_units="deg",size=6,fill_color=color)
                # fig_triangle_rl=fig.triangle([tuple[0][0]-(tuple[1]/2)],[y],line_color=color,line_width=2, muted_alpha=0.2,angle=-90,angle_units="deg",size=6,fill_color=color)
                legend_rl.append(fig_rl)
                # legend_rl.append(fig_triangle_rl)
        if l_r_distances != None:
            for tuple in l_r_distances:
                x = tuple[0][0]
                corresponding_point_x = x + tuple[1]
                y = tuple[0][1]
                text_point_x, text_point_y = (x + tuple[1] / 2, y)
                distance = int(tuple[1])
                color = "green"
                if distance == int(min_distance):
                    line_dash = 'solid'
                else:
                    line_dash = "dotted"
                fig_lr = fig.line([tuple[0][0], corresponding_point_x], [y, y], line_color=color, line_width=2,
                                  muted_alpha=0.2, line_dash=line_dash)
                # fig_triangle_lr=fig.triangle([tuple[0][0],corresponding_point_x],[y,y],line_color=color,line_width=2, muted_alpha=0.2,angle=90,angle_units="deg",size=6,fill_color=color)
                source = ColumnDataSource(data=dict(x=[tuple[0][0] + (tuple[1] / 2)], y=[y], names=[distance]))
                labels = LabelSet(x='x', y='y', text='names', level='glyph', x_offset=0, y_offset=0, source=source,
                                  render_mode='canvas')
                fig.add_layout(labels)
                legend_lr.append(fig_lr)
            #  legend_lr.append(fig_triangle_lr)
        # building the custom legend
        fig_boundary = fig.line([0, self.x_boundary, self.x_boundary, 0, 0],
                                [0, 0, self.y_boundary, self.y_boundary, 0], line_color="black", line_width=2,
                                muted_alpha=0.2, )
        items = legend_polygons + [("spine", legend_spine)]
        if l_r_distances != None:
            items.append(("distance-lr", legend_lr))
        if r_l_distances != None:
            items.append(("distance-rl", legend_rl))
        items.append(("boundary", [fig_boundary]))
        legend = Legend(items=items)
        legend.click_policy = "mute"
        fig.add_layout(legend, 'right')
        if render:
            return show(fig)
        else:
            return fig

    def plot_container_steps(self, render=True, colums=2, plot_width=600, plot_height=600):
        grid = gridplot(self.plot_steps, ncols=colums, plot_width=plot_width, plot_height=plot_height)
        min_border = (int(self.hc.min_border * 10)) / 10
        max_border = (int(self.hc.max_border * 10)) / 10
        # title = Div(
        #     text="<h1>Highclass Container {}, Polygon height: {}-{} </h1>".format(self.hc.i, min_border, max_border))
        # r = column(title, grid)
        if render:
            return show(grid)
        else:
            return grid

    #         if render:
    #             return show(grid)
    #         else:
    #             return grid

    #     def plot_container(self, sigma=None, r_l_distances=None,l_r_distances=None,min_distance=None):
    #         fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(20,12), dpi=90)
    #         #x,y= self.exterior.xy
    #         if sigma==None:
    #             sigma=self.sigma
    #         for polygon in sigma:
    #             x=[]
    #             y=[]
    #             for x_text, y_text in polygon.shell :
    #                 x.append(x_text)
    #                 y.append(y_text)
    #                 #ax.text(x_text, y_text, '({}, {})'.format(int(x_text*10)/10, int(y_text*10)/10))
    #             #print(x,y)
    #             ax.plot(x, y)
    #             ax.scatter(x, y, s=60)
    #             x1 = [x[0]for x in polygon.spine]
    #             y1= [x[1]for x in polygon.spine]
    #             ax.plot(x1,y1, linewidth=2,color='black')
    #         if self.box_boundarys_x_values!=[]:
    #             for boundary in self.box_boundarys_x_values:
    #                 ax.plot([self.box_boundarys_x_values,self.box_boundarys_x_values],[0,self.y_boundary],linestyle='--')
    #             ax.set_title('hc_{} Container -> Mini Containers'.format(self.hc.i))
    #         else:
    #             ax.set_title('hc_{} Container'.format(self.hc.i))
    #         if r_l_distances!= None:
    #             for tuple in r_l_distances:
    #                 x = tuple[0][0]
    #                 corresponding_point_x = x-tuple[1]
    #                 y = tuple[0][1]
    #                 text_point_x,text_point_y = (x-tuple[1]/2,y)
    #                 distance = int(tuple[1])
    #                 ax.text(text_point_x, text_point_y, '{}'.format(distance))
    #                 if distance==int(min_distance):
    #                     ax.plot([tuple[0][0],corresponding_point_x],[y,y],linestyle='--',marker='<',color='green')
    #                 else:
    #                     ax.plot([tuple[0][0],corresponding_point_x],[y,y],linestyle='--',marker='<',color='red')
    #         if l_r_distances!= None:
    #             for tuple in l_r_distances:
    #                 x = tuple[0][0]
    #                 corresponding_point_x = x+tuple[1]
    #                 y = tuple[0][1]
    #                 text_point_x,text_point_y = (x+tuple[1]/2,y)
    #                 distance = int(tuple[1])
    #                 ax.text(text_point_x, text_point_y, '{}'.format(distance))
    #                 if distance==int(min_distance):
    #                     ax.plot([tuple[0][0],corresponding_point_x],[y,y],linestyle='--',marker='>',color='green')
    #                 else:
    #                     ax.plot([tuple[0][0],corresponding_point_x],[y,y],linestyle='--',marker='>',color='blue')
    #         mplcursors.cursor()
    #         ax.plot([0,self.x_boundary,self.x_boundary,0,0],[0,0,self.y_boundary,self.y_boundary,0])
    #         return plt.show()

    def slope3(self, spine, point1):
        # y = m*x+n
        # (y-n)/m=x
        # n = y-m*x
        if spine[0][0] == spine[1][0]:  # Sonderfall für eine senkrechte
            distance = math.sqrt((spine[0][0] - point1[
                0]) ** 2)  # da der spine eine senkrechte ist -> der eintreffpunkt hat den gleichen y wert wie der punkt, in der Abstand formel fällt dieser wert weg
        elif spine[0][1] == point1[1]:
            distance = math.sqrt((spine[0][0] - point1[0]) ** 2 + (spine[0][1] - point1[1]) ** 2)
        else:
            slope = (spine[1][1] - spine[0][1]) / (spine[1][0] - spine[0][
                0])  # m = y2-y1/x2-x1 Formel für die Geradensteigung mithilfe aus zwei verschiedenen Punkten der Geraden
            # print("steigung", slope)
            # print(point1[1])
            n = spine[0][1] - (slope * spine[0][0])
            # print("y-achsenabnschnitt",n)
            point0_x = (point1[1] - n) / slope  # x=(y-n)/m
            # print("x-wert vom punkt",point0_x)
            point0 = (point0_x, point1[1])
            distance = math.sqrt((point0[0] - point1[0]) ** 2 + (point0[1] - point1[1]) ** 2)
        return distance

    def find_successor(self, vertex, array):
        for counter, top_edge_point in enumerate(array):
            if top_edge_point[1] >= vertex[1]:
                return (array[counter - 1], top_edge_point)

    def packing_container(self, hc_polygons):
        tree = self.Tree
        p = []
        l = []
        r = []
        b = []
        sigma_helper = []  # vi
        step_counter = 0
        for polygon in hc_polygons:
            sigma_helper.append(polygon)
            if self.sigma == []:
                transform_x = -polygon.min_x
                transform_y = -polygon.min_y
                # polygon.plot_polygon("before translation")
                polygon.translation(transform_x, transform_y)

                # polygon.plot_polygon("after translation")
                polygon_vertices = len(polygon.right)
                for count in range(0, polygon_vertices - 1):
                    vertice = polygon.right[count]
                    vertice_neighbour = polygon.right[count + 1]
                    self.root = self.Tree.insert_node(self.root, vertice[1], vertice, vertice_neighbour)
                self.sigma.append(polygon)
                self.x_boundary += polygon.width
                t = deepcopy(self.sigma)  # vi
                # t.append(polygon) # vi
                # self.plot_container(t) # vi

                self.plot_steps.append(
                    self.plot_container(sigma_helper, render=False, title="Step {}".format(step_counter)))
                step_counter += 1
            else:
                # print("self.x boundary",self.x_boundary)#p
                # if polygon.min_x < self.x_boundary:
                transform_x = (self.x_boundary + abs(polygon.min_x)) + 10
                # print("x_boundary:",self.x_boundary,"polygon min x abs",abs(polygon.min_x))#p
                # print("transform x:",transform_x)#p
                # print("polygon min x",polygon.min_x)#p
                #                 else:
                #                     transform_x=0
                transform_y = -polygon.min_y
                polygon.translation(transform_x, transform_y)
                # print(self.sigma) # vi
                min_horizontal_distance = math.inf
                # print("tree/n",tree.printHelper(self.root, "", True))#p
                helper_0 = []
                for vertex in polygon.left:
                    vertex_y = vertex[1]
                    successor_vertex = self.Tree.find_edges(self.root, vertex_y, self.root)
                    corresponding_edge_points = (successor_vertex.vertice, successor_vertex.vertice_neighbour)
                    # print("corresponding edge points",corresponding_edge_points,"for vertex",vertex)#p
                    distance = self.slope3(corresponding_edge_points, vertex)
                    if distance <= min_horizontal_distance:
                        min_horizontal_distance = distance
                    helper_0.append((vertex, distance))
                # print("min_horizontal distance right to left",min_horizontal_distance)#p
                # print("helper_0",helper_0)#p
                key = polygon.spine[1][1]
                # print("key to order tree array", key)#p
                tree_array = self.Tree.preOrder_array((self.root), [], key)
                # print("tree_array in order ",tree_array)#p
                helper_1 = []
                for vertex in tree_array:
                    # print("vertex",vertex)#p
                    # print("polygon left vertices",polygon.left)#p
                    successor = self.find_successor(vertex, polygon.left)
                    # print(successor)#p
                    distance = self.slope3(successor, vertex)
                    # print("distance",distance) #p
                    if distance <= min_horizontal_distance:
                        min_horizontal_distance = distance
                    self.root = self.Tree.delete_node(self.root, vertex[1])  # deleting vertices from B
                    helper_1.append((vertex, distance))  # -------
                #                 for count in range(0,polygon_vertices-1):
                #                     vertice = polygon.translated_right[count]
                #                     vertice_neighbour = polygon.translated_right[count+1]
                #                     self.root = self.Tree.insert_node(self.root,vertice[1],vertice,vertice_neighbour)
                # print("min_horizontal distance left to right ",min_horizontal_distance)    #p
                # self.plot_container(sigma_helper,helper_0,helper_1,min_horizontal_distance) #---------------------
                self.plot_steps.append(
                    self.plot_container(sigma_helper, helper_0, helper_1, min_horizontal_distance, render=False,
                                        title="Step {}".format(step_counter)))
                step_counter += 1
                polygon.translation(-(min_horizontal_distance), 0)
                # print(polygon.right)# p
                for counter, vertex in enumerate(polygon.right[0:-1]):
                    self.root = self.Tree.insert_node(self.root, vertex[1], vertex, (polygon.right[counter + 1]))
                x_boundary = max([vertex[0] for vertex in polygon.right] + [self.x_boundary])
                self.x_boundary = x_boundary
                self.sigma.append(polygon)
        self.plot_steps.append(self.plot_container(self.sigma, render=False, title="Finished Container"))
        return self.sigma


def building_containers(height_classes, plot=None):
    containers = []
    for height_class in height_classes:
        container = Container(height_class)
        if plot == True:
            container.packing_container(container.hc.spine_ordered_polygons)
        else:
            container.packing_container(container.hc.spine_ordered_polygons)
        containers.append(container)
    return containers


class Mini_Container(object):
    def __init__(self, max_width, max_height, max_polygon_width, hc_i):
        self.max_polygon_width = max_polygon_width
        self.current_right_boundary = 0
        self.hc_i = hc_i
        self.max_width = max_width
        self.height = max_height
        self.c = 2.214
        self.polygons = []
        self.y_offset = 0
        self.plot_steps = []

    def plot_container(self, render=True, title="", background_c=None):
        legend_polygons = []
        legend_spine = []
        legend_lr = []
        legend_rl = []
        TOOLTIPS = [("index", "$index"), ("(x,y)", "($x, $y)"), ]
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', tooltips=TOOLTIPS, toolbar_location="below")
        if background_c != None:
            # fig.background_fill_color = background_c
            # fig.background_fill_alpha = 0.1
            color_box = BoxAnnotation(left=0, right=self.max_width, bottom=self.y_offset,
                                      top=self.y_offset + self.height, fill_color=background_c,
                                      fill_alpha=0.1)
            fig.add_layout(color_box)
        for counter, polygon in enumerate(self.polygons):
            x_values = []
            y_values = []
            for x, y in polygon.shell:
                x_values.append(x)
                y_values.append(y)
            poly_fig = fig.line(x_values, y_values, line_width=2, muted_alpha=0.2)
            circle_fig = fig.circle(x_values, y_values, line_width=2, muted_alpha=0.2)
            legend_label = "Polygon{}".format(counter)
            legend_polygons.append((legend_label, [poly_fig, circle_fig]))
            x_spine = [x[0] for x in polygon.spine]
            y_spine = [y[1] for y in polygon.spine]

            spine_fig = fig.line(x_spine, y_spine, line_color="red", line_width=2, muted_alpha=0.2)
            legend_spine.append(spine_fig)
        # mini-Container helper
        # old split line

        # building the custom legend
        fig_boundary = fig.line([0, self.max_width, self.max_width, 0, 0],
                                [0 + self.y_offset, 0 + self.y_offset, self.height + self.y_offset,
                                 self.height + self.y_offset, 0 + self.y_offset]
                                , line_color="black", line_width=2, muted_alpha=0.2, )
        items = legend_polygons + [("spine", legend_spine)]
        items.append(("boundary", [fig_boundary]))
        legend = Legend(items=items)
        legend.click_policy = "mute"
        fig.add_layout(legend, 'right')
        if render:
            return show(fig)
        else:
            return fig


#     def plot_container(self,title=None):
#         #fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(20,12), dpi=90)
#         #x,y= self.exterior.xy

#         for polygon in self.polygons:
#             x=[]
#             y=[]
#             for x_text, y_text in polygon.shell :
#                 x.append(x_text)
#                 y.append(y_text)
#                 ax.text(x_text, y_text-(y_text*0.1), '({}, {})'.format(int(x_text*10)/10, int(y_text*10)/10),)
#             #print(x,y)
#             ax.plot(x, y)
#             ax.scatter(x, y, s=60)
#             if title==None:
#                 ax.set_title('Mini-Container')
#             else:
#                 ax.set_title(title)
#             x1 = [x[0]for x in polygon.spine]
#             y1= [x[1]for x in polygon.spine]
#             ax.plot(x1,y1, linewidth=2,color='black')
#         mplcursors.cursor()
#         ax.plot([0,self.max_width,self.max_width,0,0],[0+self.y_offset,0+self.y_offset,self.height+self.y_offset,self.height+self.y_offset,0+self.y_offset])
#         return plt.show()

def pack_mini_containers(container_array, plot_steps=False):
    mini_container_array = []
    plot_steps_helper_tuples = []

    colors = itertools.cycle(palette)
    background_color_list = [next(colors)]
    for container in container_array:
        mini_container_plot = []
        copy_container = container  # nochmal schauen ob der Schritt nötig ist bzw. ob nur ein Pointer kopiert wird statt eine deepcopy
        box_width = 2.214 * copy_container.hc.w_max
        box_width_helper = 0

        box_counter = 1
        # print("container boundarys",copy_container.x_boundary)
        while box_width_helper < copy_container.x_boundary:
            container.box_boundarys_x_values.append(box_width_helper)
            box_width_helper += box_width
            background_color_list.append(next(colors))
        # if plot == True:
        #     copy_container.plot_container(copy_container.sigma)
        # container.plot_steps.append(copy_container.plot_container(copy_container.sigma, render=True))
        container_plot = copy_container.plot_container(copy_container.sigma, render=False,
                                                       background_c_list=background_color_list)
        max_width_mini_container = box_width + copy_container.hc.w_max
        background_counter = 0
        while len(copy_container.sigma) > 0 and (box_width * box_counter) < (copy_container.x_boundary):
            # print("max_with,y_boundary",max_width_mini_container,(copy_container.y_boundary))
            b_color = background_color_list[background_counter]
            mini_container = Mini_Container(max_width_mini_container, (copy_container.y_boundary),
                                            copy_container.hc.w_max, copy_container.hc.i)
            translation_flag = True
            while len(copy_container.sigma) > 0 and (copy_container.sigma[0].min_x) < (box_width * box_counter):
                if translation_flag:
                    translation = copy_container.sigma[0].min_x
                    translation_flag = False
                copy_container.sigma[0].translation(-translation, 0)
                mini_container.polygons.append(copy_container.sigma.pop(0))
                # mini_container.plot_container()
            mini_container.current_right_boundary = mini_container.polygons[-1].max_x
            mini_container_array.append(mini_container)
            #             if plot==True:
            #                 title = "Mini-Container {} (hc_{})".format(box_counter, container.hc.i)
            #                 mini_container.plot_container(title)
            title = "Mini-Container {} (hc_{})".format(box_counter, container.hc.i)
            mini_container_plot.append(mini_container.plot_container(title=title, render=False, background_c=b_color))
            box_counter += 1
            background_counter += 1
        if len(copy_container.sigma) > 0:
            mini_container = Mini_Container(max_width_mini_container, copy_container.y_boundary,
                                            copy_container.hc.w_max, copy_container.hc.i)
            translation_flag = True
            while len(copy_container.sigma) > 0 and copy_container.sigma[0].min_x < box_width * box_counter:
                if translation_flag:
                    translation = copy_container.sigma[0].min_x
                    translation_flag = False
                copy_container.sigma[0].translation(-translation, 0)
                mini_container.polygons.append(copy_container.sigma.pop(0))
                # mini_container.plot_container()
            mini_container.current_right_boundary = mini_container.polygons[-1].max_x
            mini_container_array.append(mini_container)
            # if plot==True:
            title = "Mini-Container {} (hc_{})".format(box_counter, container.hc.i)
            # mini_container.plot_container(title)
            mini_container_plot.append(mini_container.plot_container(title=title, render=False))
        plot_steps_helper_tuples.append((container_plot, mini_container_plot))
    if plot_steps == True:
        return (mini_container_array, plot_steps_helper_tuples)
    else:
        return mini_container_array


def plot_mini_containers(plot_steps, render=True, plot_width=600, plot_height=600):
    plots = []

    # hc_container =plot_steps[0]
    for plot_container in plot_steps:
        plot_hc = [plot_container[0]] + plot_container[1]
        grid = gridplot(plot_hc, ncols=len(plot_container[1]) + 1, plot_width=plot_width, plot_height=plot_height)
        # #         show(plot1)
        #         show(grid)

        # plots.append(grid)
        # grid=row(plot_container[1])
        plots.append(grid)
        # plots.append(grid)
    if render:
        return show(layout(plots))
    else:
        return plots


class End_Container(object):
    def __init__(self, mini_container_array, angle=0):
        self.mini_containers = mini_container_array
        self.pack_mini_containers()
        self.container_area = self.container_area(self.mini_containers)
        self.angle = angle

    def pack_mini_containers(self):
        y_offset = self.mini_containers[0].height
        for mini_container in self.mini_containers[1:]:
            for polygon in mini_container.polygons:
                polygon.translation(0, y_offset)
            mini_container.y_offset = y_offset
            y_offset += mini_container.height
        return

    def container_area(self, mini_containers):
        container_area = 0
        for mini_container in mini_containers:
            container_area += mini_container.max_width * mini_container.height
        return container_area

    def plot_container(self, title="", render=True):
        legend_polygons = []
        legend_spine = []
        legend_lr = []
        legend_rl = []
        # fig = plt.subplots(facecolor='w', edgecolor='k', figsize=(20,12), dpi=90)
        TOOLTIPS = [("index", "$index"), ("(x,y)", "($x, $y)"), ]
        if title == "":
            title = 'End-Container  area:{}'.format((int(self.container_area/10))*10)
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', toolbar_location="below", tooltips=TOOLTIPS)
        y_offset = 0
        colors = itertools.cycle(palette)
        for counter, mini_container in enumerate(self.mini_containers):
            color = next(colors)
            for polygon in mini_container.polygons:
                # mapper = linear_cmap(field_name='x', palette=Viridis256 ,low=min(x) ,high=max(x))
                source = ColumnDataSource(data=dict(x=polygon.x_values, y=polygon.y_values))
                spine_x_values = [x[0] for x in polygon.spine]
                spine_y_values = [x[1] for x in polygon.spine]

                legend_label = "Mini-Container {} (hc{})".format(counter, mini_container.hc_i)
                fig.line(x="x", y="y", line_width=2, line_color=color, muted_alpha=0.2, source=source,
                         legend_label=legend_label)
                # fig.circle(x="x",y="y",fill_color=linear_cmap(field_name='y', palette= 'Viridis256'),source=source, line_width=2, muted_alpha=0.2,size=8)
                fig.circle(x="x", y="y", line_width=2, color=color, fill_color=color, muted_alpha=0.2, size=8,
                           source=source, legend_label=legend_label)
                # ax.scatter(x, y, s=60)
                fig.line(spine_x_values, spine_y_values, line_color="red", legend_label="Spine", line_width=2,
                         muted_alpha=0.2)
                fig.legend.click_policy = "mute"
            color_box = BoxAnnotation(left=0, right=mini_container.max_width, bottom=mini_container.y_offset,
                                      top=mini_container.y_offset + mini_container.height, fill_color=color,
                                      fill_alpha=0.1)
            fig.add_layout(color_box)
            fig.line([0, mini_container.max_width, mini_container.max_width, 0, 0],
                     [0 + y_offset, 0 + y_offset, mini_container.height + y_offset, mini_container.height + y_offset,
                      0 + y_offset], line_color=color, muted_alpha=0.2, legend_label=legend_label)
            y_offset += mini_container.height
        # mplcursors.cursor()
        if render:
            return show(fig)
        else:
            return fig