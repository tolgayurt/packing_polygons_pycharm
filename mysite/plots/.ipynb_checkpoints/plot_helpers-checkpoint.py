import math
from django.shortcuts import render
from plotly.offline import plot
import plots.polygon as poly
from django.http import JsonResponse
from plotly.graph_objs import Scatter
import plotly.graph_objects as go
from django.http import HttpResponse
import pdb; pdb.set_trace
from plotly.subplots import make_subplots
import math

def polygon_plot(polygons):
    plot_polygon_list = []

    polygon_count = len(polygons)
    cols = 4
    rows = math.ceil(polygon_count / cols)
    number = 0
    sub_plot_titles = ['{}   height:{}  slope:{}'.format(number,
                                                         (int(polygons[number].height * 10)) / 10,
                                                         (int(polygons[number].slope * 10)) / 10)
                        for number in range(0, polygon_count)]

    fig = make_subplots(rows=rows, cols=cols, subplot_titles=sub_plot_titles)
    fig.update_layout(title="Convex Polygons")
    counter = 0
    for polygon in polygons:
        x_data = polygon.x_values
        y_data = polygon.y_values
        scatter = go.Scatter(x=x_data, y=y_data,
                             mode='lines', name='{}'.format(counter),
                             opacity=0.8, marker_color='green')

        spine_x_values = [x[0] for x in polygon.spine]
        spine_y_values = [x[1] for x in polygon.spine]
        scatter_spine = go.Scatter(x=spine_x_values, y=spine_y_values,
                                   mode='lines', name='{} Spine'.format(counter),
                                   opacity=0.8, marker_color='red')
        row = math.ceil((counter + 1) / cols)
        col = (counter % cols) + 1
        fig.add_trace(scatter, row=row, col=col)
        fig.add_trace(scatter_spine, row=row, col=col)
        counter += 1
    plot_polygons_div = plot(fig, output_type='div')
    return plot_polygons_div

def high_class_plot(high_classes):
    plot_high_class_list = []
    polygon_number = 0
    polygon_title_number = 0
    for hc in high_classes:
        polygon_count = len(hc.polygons)
        cols = 4
        rows = math.ceil(polygon_count / cols)

        # sub_plot_titles = ['{}   height:{}  slope:{}'.format(number,
        #                                                      (int(hc.spine_ordered_polygons[number].height * 10)) / 10,
        #                                                      (int(hc.spine_ordered_polygons[number].slope * 10)) / 10)
        #                     for number in range(0, polygon_count)]
        sub_plot_titles=[]
        for polygon in hc.spine_ordered_polygons:

            sub_plot_titles.append('{}   height:{}  slope:{}'.format(polygon_title_number, (int(polygon.height*10))/10,(int(polygon.slope * 10)) / 10))
            polygon_title_number += 1
        fig = make_subplots(rows=rows, cols=cols, subplot_titles=sub_plot_titles)
        fig.update_layout(title="Highclass {}   heights: {}-{}".format(hc.i, int(hc.min_border), int(hc.max_border)))
        counter = 0

        for polygon in hc.spine_ordered_polygons:
            x_data = polygon.x_values
            y_data = polygon.y_values
            scatter = go.Scatter(x=x_data, y=y_data,
                                 mode='lines', name='{}'.format(polygon_number),
                                 opacity=0.8, marker_color='green')

            spine_x_values = [x[0] for x in polygon.spine]
            spine_y_values = [x[1] for x in polygon.spine]
            scatter_spine = go.Scatter(x=spine_x_values, y=spine_y_values,
                                       mode='lines', name='{} Spine'.format(polygon_number),
                                       opacity=0.8, marker_color='red')
            row = math.ceil((counter + 1) / cols)
            col = (counter % cols) + 1
            fig.add_trace(scatter, row=row, col=col)
            fig.add_trace(scatter_spine, row=row, col=col)
            counter += 1
            polygon_number += 1
        plt_div = plot(fig, output_type='div')
        plot_high_class_list.append(plt_div)
    return plot_high_class_list


