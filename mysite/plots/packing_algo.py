"""This module represents a approximation algorithm for the optimal packing of convex polygons.

The guideline and heart of this algorithm is the paper:

    Alt H., de Berg M., Knauer C. (2015)
    Approximating Minimum-Area Rectangular and Convex Containers for Packing Convex Polygons.
    In: Bansal N., Finocchi I. (eds) Algorithms - ESA 2015. Lecture Notes in Computer Science,
    vol 9294. Springer, Berlin, Heidelberg. https://doi.org/10.1007/978-3-662-48350-3_3

    DOI:
        https://doi.org/10.1007/978-3-662-48350-3_3
"""

import copy
import numpy
import math
import itertools

# tree data structure
from . import avl_tree

# shapely/polygon packages
from shapely.geometry import Polygon
from shapely import affinity

# plotting packages
from bokeh.plotting import figure, Figure
from bokeh.io import show
from bokeh.layouts import gridplot, layout, column, Column, Row
from bokeh.models import Div
from bokeh.models import Legend
from bokeh.models import BoxAnnotation
from bokeh.models.widgets import Tabs, Panel
from bokeh.models import ColumnDataSource, LabelSet
from bokeh.palettes import Dark2_5 as palette

# Type aliases for the type hints
Point_xy = (float, float)


class ConvexPolygon(object):
    """A convex polygon which is represented by its shell, the vertices are clockwise ordered.

    Args:
        shell ([Point_xy]): a list of (x,y) points

    Attributes:
        __shell ([Point_xy]): list of clockwise ordered convex (x,y) coordinate points which represents the polygon,
                              the shells first point and end point are closing the polygon and are the same, the shell
                              will have at least 3 Points
        __area (float): the area of the polygon
        __x_values ([float]): x coordinate values
        __y_values ([float]): y coordinate values
        __min_x (float): min x coordinate value
        __max_x (float): max x coordinate value
        __min_y (float): min y coordinate value
        __max_y (float): max y coordinate value
        __height (float): height
        __width (float): width
        __spine ((Point_xy, Point_xy)): a tuple of the vertices with the smallest and biggest y value. if there are two
                vertices which have the same smallest y value or biggest y value the left most vertex and the top right
                vertex will build the spine
        __vertices_left_visible ([Point_xy]): vertices which can be seen from left
        __vertices_right_visible ([Point_xy]): vertices which can be seen from right
        __slope (float): the slope of the spine points
        plot_fill (bool): a flag which signals that the plot of the polygon should be filled
    """
    def __init__(self, shell: [Point_xy]) -> None:
        self.__shell, self.__area = self.convex_hull_and_area(shell)
        self.__x_values = [vertex[0] for vertex in self.shell]
        self.__y_values = [vertex[1] for vertex in self.shell]
        self.__min_x = min(self.__x_values)
        self.__max_x = max(self.__x_values)
        self.__min_y = min(self.__y_values)
        self.__max_y = max(self.__y_values)
        self.__height = self.__max_y - self.__min_y
        self.__width = self.__max_x - self.__min_x
        self.__spine = self.set_spine()
        self.__vertices_left_visible, self.__vertices_right_visible = self.splitter()
        self.__slope = self.set_slope()
        self.plot_fill = False

    # all these @property decorators helps to make the attributes "private"
    @property
    def area(self):
        return self.__area

    @property
    def shell(self):
        return self.__shell

    @property
    def min_x(self):
        return self.__min_x

    @property
    def max_x(self):
        return self.__max_x

    @property
    def min_y(self):
        return self.__min_y

    @property
    def max_y(self):
        return self.__max_y

    @property
    def x_values(self):
        return self.__x_values

    @property
    def y_values(self):
        return self.__y_values

    @property
    def height(self):
        return self.__height

    @property
    def width(self):
        return self.__width

    @property
    def spine(self):
        return self.__spine

    @property
    def vertices_left_visible(self):
        return self.__vertices_left_visible

    @property
    def vertices_right_visible(self):
        return self.__vertices_right_visible

    @property
    def slope(self):
        return self.__slope

    @staticmethod
    def convex_hull_and_area(shell: [Point_xy]) -> ([Point_xy], float):
        """Builds the convex hull of the input points and creates a convex polygon with his area

        This function creates a shapely (a python package) polygon to use the shapely convex hull function,
        after the convex hull function is used the new convex vertices and the area of the polygon will be returned,
        the convex vertices are clockwise ordered and the start and end point are the same,
        this way of using the convex hull creates a redundancy and dependency of the shapely package

        Args:
            shell ([Point_xy]): the input (x,y) coordinates for initialising the convex polygon

        Returns:
            shell, area ([Point_xy], float): a tuple of the convex polygon shell and polygon area
        """
        if shell is None:
            raise ValueError("a convex polygon can't intialized with None")
        elif shell == [] or len(shell) < 3:
            raise ValueError("a convex polygon needs at least 3 points")
        shapely_polygon = Polygon(shell)
        convex_polygon = shapely_polygon.convex_hull
        if type(convex_polygon) is not Polygon:
            raise ValueError(
                "couldn't create the convex Polygon, to less points after using the convex hull on the input points")
        shell = list(convex_polygon.exterior.coords)
        area = convex_polygon.area
        return shell, area

    def translation(self, x: float, y: float) -> None:
        """Translates the polygon with the given input x and y values.

        All dependent attributes of the convex polygon will be updated.

        Args:
            x (float): value to translate the polygon in x coordinate direction
            y (float): value to translate the polygon in y coordinate direction

        Returns:
            None: just the attributes of the polygon will be updated
        """
        self.__shell = [(point[0] + x, point[1] + y) for point in self.__shell]
        self.__spine = ((self.__spine[0][0]+x, self.__spine[0][1]+y), (self.__spine[1][0]+x, self.__spine[1][1]+y))
        self.__vertices_left_visible = [(point[0] + x, point[1] + y) for point in self.__vertices_left_visible]
        self.__vertices_right_visible = [(point[0] + x, point[1] + y) for point in self.__vertices_right_visible]
        self.__min_x = self.__min_x + x
        self.__max_x = self.__max_x + x
        self.__min_y = self.__min_y + y
        self.__max_y = self.__max_y + y
        self.__x_values = [vertex + x for vertex in self.__x_values]
        self.__y_values = [vertex + y for vertex in self.__y_values]

    def set_spine(self) -> (Point_xy, Point_xy):
        """Builds the spine, which slope will be important to order the polygons later

        A tuple of the vertices with the smallest and biggest y value. if there are two vertices which have the same
        smallest y value or biggest y value the left most vertex and the top right vertex will build the spine.
        The spine also splits the polygon in from left_visible_vertices and from right_visible_vertices

        Returns:
           spine_bottom, spine_top (Point_xy, Point_xy): a tuple a the lowest and highest vertex or bottom left and
                                                         top left
        """
        y_sorted_p_p = sorted(self.__shell[0:-1], key=lambda k: k[1])  # ordering the vertices to their y values
        if y_sorted_p_p[0][1] == y_sorted_p_p[1][1]:  # if there are two lowest vertices take the left one
            if y_sorted_p_p[0][0] < y_sorted_p_p[1][0]:
                spine_bottom = y_sorted_p_p[0]
            else:
                spine_bottom = y_sorted_p_p[1]
        else:
            spine_bottom = y_sorted_p_p[0]
        if y_sorted_p_p[-2][1] == y_sorted_p_p[-1][1]:  # if there are two highest vertices take right one
            if y_sorted_p_p[-2][0] < y_sorted_p_p[-1][0]:
                spine_top = y_sorted_p_p[-1]
            else:
                spine_top = y_sorted_p_p[-2]
        else:
            spine_top = y_sorted_p_p[-1]
        return spine_bottom, spine_top

    def set_slope(self) -> float:
        """Builds the slope for the spine vertices

        The slope will be build with m = y2-y1/x2-x1.

        Returns:
            slope (float): returns the slope of the spine vertices
        """
        spine = self.__spine
        if spine[0][0] == spine[1][0]:  # case for a vertical spine
            slope = math.inf
        else:
            slope = (spine[1][1] - spine[0][1]) / (spine[1][0] - spine[0][0])
        return slope

    def plot_polygon(self, title="", plot_width=400, plot_height=300, color="#1F77B4", render=True) -> Figure:
        """Plot object of the polygon

        Args:
            title (str): the title for the plot
            plot_width (int): the width of the plot
            plot_height (int): the height of the plot
            color (str): the fill_color of the polygon
            render (bool): If True the polygon will also be rendered else not

        Returns:
            fig (Figure): a plot Figure object which can be shown with bokeh command show(fig)
        """
        legend_items = []
        height = (int(self.__height * 10)) / 10
        if self.__slope == math.inf:
            slope = math.inf
        else:
            slope = truncate(self.__slope, 1)
        x_data = self.__x_values
        y_data = self.__y_values
        tooltips = [("index", "$index"), ("(x,y)", "($x{0.0}, $y{0.0})"), ]
        if title == "":
            title = 'height: {}  slope: {}'.format(height, slope)
        else:
            title = '{}  height: {}  slope: {}'.format(title, height, slope)
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', width=plot_width, height=plot_height,
                     tooltips=tooltips, )
        if self.plot_fill:
            poly_fig = fig.patch(x_data, y_data, line_width=1, color=color, alpha=0.9, line_alpha=1, muted_alpha=0.05)
        else:
            poly_fig = fig.patch(x_data, y_data, line_width=1, alpha=0.1, color=color, line_alpha=1, muted_alpha=0.05)
        fig_points = fig.circle(x_data, y_data, color=color, line_width=1, muted_alpha=0.02, size=4)
        legend_items.append(("shell-lines", [poly_fig]))
        legend_items.append(("vertices", [fig_points]))
        spine_x_values = [x[0] for x in self.__spine]
        spine_y_values = [x[1] for x in self.__spine]
        fig_spine = fig.line(spine_x_values, spine_y_values, line_color="red", line_width=1, muted_alpha=0.2,
                             muted_color="red")
        legend_items.append(("spine", [fig_spine]))
        legend = Legend(items=legend_items)
        legend.click_policy = "mute"
        fig.add_layout(legend, 'right')
        if render:
            show(fig)
        return fig

    def splitter(self) -> ([Point_xy], [Point_xy]):
        """Splits the vertices into vertices_visible from left and vertices_visible from right

        This splitting is important for the upcoming packing steps, when the polygons will be filled into high
        class containers

        Returns:
            left, right (([Point_xy], [Point_xy])): a tuple of the list of vertices visible from left and a list of
                                                    vertices visible from right
        """
        left = []
        spine_bottom = self.__spine[0]
        spine_top = self.__spine[1]
        help_array = self.__shell[:]
        if help_array[0] == help_array[-1]:  # do not need the duplicated/connecting last vertex of the shell
            help_array = help_array[0:-1]
        spine_bottom_found = False
        spine_top_found = False
        spine_bottom_not_horizontal = False
        # cycling threw the vertices and searching for the spine bottom, which will be the start point
        while not spine_bottom_found:
            if help_array[0] == spine_bottom:
                # if the bottom_spine_vertex has a horizontal neighbour, do not take it to the right_visible_vertices
                if spine_bottom[1] != help_array[-1][1]:
                    spine_bottom_not_horizontal = True
                spine_bottom_found = True
                left.append(help_array.pop(0))
            else:
                help_array.append(help_array.pop(0))
        while not spine_top_found:  # searching the spine top
            if help_array[0] == spine_top:
                spine_top_found = True
                # if the top_spine_vertex has a horizontal neighbour, do not take it to the left_visible_vertices
                if spine_top[1] != left[-1][1]:
                    left.append(spine_top)
            else:
                left.append(help_array.pop(0))
        right = help_array.copy()
        if spine_bottom_not_horizontal:
            right.append(spine_bottom)
        return left, right


class HighClass(object):
    """Holds the polygons which have a height between the HighClass self.min_border < polygon.height <= self.max_border

    Args:
        i (int): represent the HighClass index
        alpha (float): value which builds the HighClass borders
        h_max_polygon (float): max height from all polygons to pack (not only from this HighClass)
        w_max_polygon  (float):  max width from all polygons to pack (not only from this HighClass)

    Attributes:
        i (int): represent the HighClass index
        alpha (float): value which builds the HighClass borders
        h_max_polygon (float): max height from all polygons to pack (not only from this HighClass)
        w_max_polygon  (float):  max width from all polygons to pack (not only from this HighClass)
        min_border (float): min border of the HighClass, only Polygons with height bigger will join this HighClass
        max_border (float): max border of the HighClass, only Polygons with height smaller or equal will join this
                           HighClass
        polygons ([ConvexPolygon]): Convex Polygons which fulfill the min_border and max_border condition
        spine_ordered_polygons ([ConvexPolygon): the self.polygons ordered by their spines in ascending order
    """
    def __init__(self, i: int, alpha: float, h_max_polygon: float, w_max_polygon: float) -> None:
        self.i = i
        self.alpha = alpha
        self.h_max_polygon = h_max_polygon
        self.w_max_polygon = w_max_polygon
        self.min_border = alpha ** (i + 1) * h_max_polygon
        self.max_border = alpha ** i * h_max_polygon
        self.polygons = []
        self.spine_ordered_polygons = []

    def set_polygons(self, polygons: [ConvexPolygon]) -> None:
        """Set the polygons and the spine_ordered_polygons which fulfill the HighClass border conditions

        This function will be triggered by the function which creates the HighClass objects.

        Args:
            polygons ([ConvexPolygon): these convex polygons fulfill the HighClass border conditions
        """
        self.polygons = polygons
        spine_ordered_polygons = sorted(self.polygons, key=lambda polygon: polygon.slope)
        self.spine_ordered_polygons = spine_ordered_polygons

    def plot_hc(self, ordered=False, render=True, plot_width=300, plot_height=300, columns=4) -> Column:
        """Plot object of the polygons of the HighClass with ordered or unordered spines

        Args:
            ordered (bool): if True -> polygons will be spine ordered else not ordered polygons
            render (bool): if True -> the plot will be rendered else not
            plot_width (int): width of the plot figure
            plot_height (int): height of the plot figure
            columns (int): the number of columns in which the plot is shown
        Returns:
            grid_layout (Column): plot object which can be rendered with show(grid_layout)
        """
        plots = []
        if ordered:
            polygon_list = self.spine_ordered_polygons
        else:
            polygon_list = self.polygons
        for counter, polygon in enumerate(polygon_list):
            title = "Polygon {}".format(counter)
            plots.append(polygon.plot_polygon(title=title, render=False))
        grid = gridplot(plots, ncols=columns, plot_width=plot_width, plot_height=plot_height, toolbar_location="left")
        grid_title = Div(text="<b>Hc_{}&nbsp;&nbsp;height:&nbsp;{}-{}&nbsp;&nbsp;alpha: {}<b>".format(self.i, truncate(
            self.min_border, 1), truncate(self.max_border, 1), self.alpha))
        grid_layout = column(grid_title, grid)
        if render:
            show(grid_layout)
        return grid_layout


class Container(object):
    """Container which holds the packed polygons from one HighClass

    The Container has fixed height which is dependent from the HighClass max_boundary.
    The polygon which will get packed (included/translated) will be in ascending order according to their spine value.

    Args:
        hc (HighClass): reference to HighClass object
        y_boundary (float): y coordinate boundary for the container dependent of the High_Class max_boundary
        x_boundary (float): x coordinate boundary fot the container
        Tree (Tree): avl-tree data structure
        root (TreeNode): the root of the avl-tree data structure
        sigma ([ConvexPolygon]): the list of packed convex polygons from the HighClass
        plot_steps ([Figure]): list of steps which are done to pack the container as plot objects

    Attributes:
        hc (HighClass): reference to HighClass object
    """
    def __init__(self, hc: HighClass) -> None:
        self.hc = hc
        self.y_boundary = self.hc.max_border
        self.x_boundary = 0
        self.Tree = avl_tree.AVLTree()
        self.root = None
        self.sigma, self.plot_steps = self.packing_container(self.hc.spine_ordered_polygons)

    @staticmethod
    def distance(edge_points: (Point_xy, Point_xy), point: Point_xy) -> float:
        """Calculates the shortest distance between an edge and a point

        Args:
            edge_points ((Point_xy, Point_xy)): edge of a polygon
            point (): vertex from there the shortest distance to the edge will be calculated

        Returns:
            distance (float): the shortest distance betwee the vertex and edge
        """
        edge_p1 = edge_points[0]
        edge_p2 = edge_points[1]
        if edge_p1[0] == edge_p2[0]:  # if edge is a vertical
            distance = math.sqrt((edge_p1[0] - point[0]) ** 2)
        elif edge_p1[1] == point[1]:   # if the first edge vertex and the vertex have the same y value
            distance = math.sqrt((edge_p1[0] - point[0]) ** 2 + (edge_p1[1] - point[1]) ** 2)
        else:
            slope = (edge_p2[1] - edge_p1[1]) / (edge_p2[0] - edge_p1[0]) # m = y2-y1/x2-x1
            # y = m*x+n used the Equation of a straight line and transformed it to get the missing parameters
            n = edge_p1[1] - (slope * edge_p1[0])  # n = y - (m*x)
            intersection_point_x = (point[1] - n) / slope  # x = (y-n)/m
            intersection_point = (intersection_point_x, point[1])
            distance = math.sqrt((intersection_point[0] - point[0]) ** 2 + (intersection_point[1] - point[1]) ** 2)
        return distance

    @staticmethod
    def find_corresponding_edge(vertex: Point_xy, vertices_visible_left: [Point_xy]) -> (Point_xy, Point_xy):
        """This function finds the corresponding edge for a vertex which is already packed and visible from right.

        To find the corresponding edge the vertices visible from left of the polygon which will be moved to the left
        will be checked. Important is that the vertices_visible_left are ordered from smallest y values to bigger ones.
        This condition is guaranteed by the splitter function which builds the vertices_visible_left.
        The first vertex in the vertices_visible_left list is the bottom_spine element which has the smallest y value
        and the next element are ordered because the polygon vertices are convex and where build clockwise.
        Args:
            vertex (Point_xy): a vertex which is already in the container and visible from the right
            vertices_visible_left ([Point_xy]): vertices visible from left of the polygon which will be move to the left

        Returns:
            bottom_edge_vertex, top_edge_vertex (Point_xy,Point_xy): the corresponding edge with his two building points
        """
        for counter, top_edge_vertex in enumerate(vertices_visible_left):
            if top_edge_vertex[1] >= vertex[1]:
                bottom_edge_vertex = vertices_visible_left[counter - 1]
                return bottom_edge_vertex, top_edge_vertex

    def packing_container(self, hc_spine_ordered_polygons: [ConvexPolygon]) -> ([ConvexPolygon], [Figure]):
        """Packs the spine ordered polygons into the container by translating them to the correct place

        This function will be called by initializing the Container object.
        It uses a balanced tree as data structure which helps to pack the polygons efficient into the container.
        The tree structure needs be updated while packing and holds all currently packed vertices which are visible
        from right.
        The function also creates plot objects for almost every packing step.

        Args:
            hc_spine_ordered_polygons ([ConvexPolygon]): spine ordered polygons from the HighClass to pack

        Returns:
            sigma, plot_steps (([ConvexPolygon], [Figure])): a tuple of the packed polygons and a list of the plot
                                                             objects which represent a packing step
        """
        # backup for the ordered spine HighClass polygons because they will be translated
        hc_polygons = copy.deepcopy(hc_spine_ordered_polygons)
        sigma_plot_helper = []
        step_counter = 0
        sigma = []
        plot_steps = []
        for polygon in hc_polygons:
            sigma_plot_helper.append(polygon)
            if not sigma:   # first polygon will be packed
                transform_x = -polygon.min_x
                transform_y = -polygon.min_y
                polygon.translation(transform_x, transform_y)
                polygon_vertices = len(polygon.vertices_right_visible)
                # filling the tree with all vertices visible form right of the first polygon
                for count in range(0, polygon_vertices - 1):
                    vertex = polygon.vertices_right_visible[count]
                    vertex_neighbour = polygon.vertices_right_visible[count + 1]
                    self.root = self.Tree.insert_node(self.root, vertex[1], vertex, vertex_neighbour)
                sigma.append(polygon)
                self.x_boundary += polygon.width
                plot_steps.append(self.plot_container(sigma_plot_helper, render=False,
                                                      title="Step {}".format(step_counter)))
                step_counter += 1
            else:
                transform_x = (self.x_boundary + abs(polygon.min_x)) + 10
                transform_y = -polygon.min_y
                polygon.translation(transform_x, transform_y)
                min_horizontal_distance = math.inf
                distance_rl_plot_helper = []
                # find the smallest distance from right to left
                for vertex in polygon.vertices_left_visible:
                    vertex_y = vertex[1]
                    # finds the next biggest node (vertex) in the tree which holds his smaller neighbour point (vertex)
                    successor_vertex = self.Tree.find_edges(self.root, vertex_y, self.root)
                    corresponding_edge_points = (successor_vertex.vertex, successor_vertex.vertex_neighbour)
                    distance = self.distance(corresponding_edge_points, vertex)
                    if distance <= min_horizontal_distance:
                        min_horizontal_distance = distance
                    distance_rl_plot_helper.append((vertex, distance))
                key = polygon.spine[1][1]
                tree_array = self.Tree.preOrder_array(self.root, [], key)
                # tree_array holds all vertices which are already packed, visible from right and can hit the new
                # polygon which will be packed to the container
                distance_lr_plot_helper = []
                # find the smallest distance from left to right
                for vertex in tree_array:
                    successor = self.find_corresponding_edge(vertex, polygon.vertices_left_visible)
                    distance = self.distance(successor, vertex)
                    if distance <= min_horizontal_distance:
                        min_horizontal_distance = distance
                    # deleting the vertex because the new polygon will cover over it
                    self.root = self.Tree.delete_node(self.root, vertex[1])
                    distance_lr_plot_helper.append((vertex, distance))
                plot_steps.append(
                    self.plot_container(sigma_plot_helper, distance_rl_plot_helper, distance_lr_plot_helper,
                                        min_horizontal_distance, render=False, title="Step {}".format(step_counter)))
                step_counter += 1
                # after the smallest translation distance is found the polygon will be packed and the tree updated
                polygon.translation(-min_horizontal_distance, 0)
                # add the right_visible vertices from the new packed polygon into the tree
                for counter, vertex in enumerate(polygon.vertices_right_visible[0:-1]):
                    self.root = self.Tree.insert_node(self.root, vertex[1], vertex,
                                                      (polygon.vertices_right_visible[counter + 1]))
                x_boundary = max([vertex[0] for vertex in polygon.vertices_right_visible] + [self.x_boundary])
                self.x_boundary = x_boundary
                sigma.append(polygon)
        plot_steps.append(self.plot_container(sigma, render=False, title="Finished Container"))
        return sigma, plot_steps

    def plot_container(self, sigma=None, r_l_distances=None, l_r_distances=None, min_distance=None, render=True,
                       title="", box_boundaries_x_values_colors_tuple=None) -> Figure:
        """Creates a plot object of the actual container

        Args:
            sigma ([ConvexPolygon]): ConvexPolygons which are already packed into the container
            r_l_distances ([(Point_xy, float)]): the tuples represent the vertices visible from left of the polygon
                                                 to pack and their distances to the corresponding edges
            l_r_distances ([(Point_xy, float)]): the tuples represent the vertices visible from right of all already
                                                 packed polygons and their distances to the corresponding edges
            min_distance (float): the minimal distance from all distances right to left and left to right
            render (bool): if True the plot will be also rendered else not
            title (str): the title of the plot default ""
            box_boundaries_x_values_colors_tuple (([float],[color])): the boundary x values for the boxes and their
                                                                     background color, the boxes will later be
                                                                     extended to the mini-containers
        Returns:
            fig (Figure): a plot object of the container
        """
        legend_items = []
        legend_polygons = []
        legend_spine = []
        legend_lr = []
        legend_rl = []
        legend_vertices = []
        if sigma is None:
            sigma = self.sigma
        tooltips = [("index", "$index"), ("(x,y)", "($x{0.0}, $y{0.0})"), ]
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', tooltips=tooltips, toolbar_location="left")
        # builds the polygon plots
        for counter, polygon in enumerate(sigma):
            x_values = polygon.x_values
            y_values = polygon.y_values
            poly_fig = fig.patch(x_values, y_values, line_width=1, alpha=0.1, muted_alpha=0.05, line_alpha=1)
            circle_fig = fig.circle(x_values, y_values, line_width=1, muted_alpha=0.01)
            legend_label = "Polygon{}".format(counter)
            legend_polygons.append((legend_label, [poly_fig]))
            legend_vertices.append(circle_fig)
            x_spine = [x[0] for x in polygon.spine]
            y_spine = [y[1] for y in polygon.spine]
            spine_fig = fig.line(x_spine, y_spine, line_color="red", line_width=1, alpha=0.01, muted_color="red",
                                 muted_alpha=1)
            legend_spine.append(spine_fig)
        # mark the boxes  with their background color, these boxes will later be extend to mini-containers
        if title == "":
            fig.title.text = 'Hc_{}-Container height: {} '.format(self.hc.i, truncate(self.y_boundary, 1))
        if box_boundaries_x_values_colors_tuple is not None:
            box_boundaries_x_values = box_boundaries_x_values_colors_tuple[0]
            background_c_list = box_boundaries_x_values_colors_tuple[1]
            for counter, boundary in enumerate(box_boundaries_x_values[0:-1]):
                next_boundary = box_boundaries_x_values[counter + 1]
                color_box = BoxAnnotation(bottom=0, top=self.y_boundary, left=box_boundaries_x_values[counter],
                                          right=next_boundary, fill_color=background_c_list[counter], fill_alpha=0.1)
                fig.add_layout(color_box)
                fig.line([next_boundary, next_boundary], [0, self.y_boundary], line_dash="dotted")
            fig.title.text = 'Hc_{}-Container to Mini Containers'.format(self.hc.i, truncate(self.y_boundary, 1))
        if r_l_distances is not None:
            for vertex_distance_tuple in r_l_distances:
                vertex_r = vertex_distance_tuple[0]
                distance = vertex_distance_tuple[1]
                x = vertex_r[0]
                corresponding_point_x = x - distance
                y = vertex_r[1]
                distance_int = int(distance)
                color = "blue"
                if distance_int == int(min_distance):
                    line_dash = "solid"
                else:
                    line_dash = "dotted"
                fig_rl = fig.line([vertex_r[0], corresponding_point_x], [y, y], line_color=color, line_width=1,
                                  muted_alpha=0.2, line_dash=line_dash)
                source = ColumnDataSource(data=dict(x=[vertex_r[0] - (distance / 2)], y=[y], names=[distance_int]))
                labels = LabelSet(x='x', y='y', text='names', level='glyph', x_offset=0, y_offset=0, source=source,
                                  render_mode='canvas')
                fig.add_layout(labels)
                legend_rl.append(fig_rl)
        if l_r_distances is not None:
            for vertex_distance_tuple in l_r_distances:
                vertex_l = vertex_distance_tuple[0]
                distance = vertex_distance_tuple[1]
                x = vertex_l[0]
                corresponding_point_x = x + distance
                y = vertex_l[1]
                distance_int = int(distance)
                color = "green"
                if distance_int == int(min_distance):
                    line_dash = 'solid'
                else:
                    line_dash = "dotted"
                fig_lr = fig.line([vertex_l[0], corresponding_point_x], [y, y], line_color=color, line_width=1,
                                  muted_alpha=0.2, line_dash=line_dash)
                source = ColumnDataSource(data=dict(x=[vertex_l[0] + (distance / 2)], y=[y], names=[distance_int]))
                labels = LabelSet(x='x', y='y', text='names', level='glyph', x_offset=0, y_offset=0, source=source,
                                  render_mode='canvas')
                fig.add_layout(labels)
                legend_lr.append(fig_lr)
        # building the custom legend
        fig_boundary = fig.line([0, self.x_boundary, self.x_boundary, 0, 0],
                                [0, 0, self.y_boundary, self.y_boundary, 0], line_color="black", alpha=0.7,
                                line_width=1, muted_alpha=0.2, )
        legend_items.append(("spine", legend_spine))
        if l_r_distances is not None:
            legend_items.append(("distance-lr", legend_lr))
        if r_l_distances is not None:
            legend_items.append(("distance-rl", legend_rl))
        legend_items.append(("boundary", [fig_boundary]))
        legend_items.append(("vertices", legend_vertices))
        legend_items = legend_items + legend_polygons
        legend = Legend(items=legend_items)
        legend.click_policy = "mute"
        fig.add_layout(legend, 'right')
        if render:
            show(fig)
        return fig

    def plot_container_steps(self, render=True, columns=2, plot_width=600, plot_height=600) -> Column:
        """Build a plot object which includes all container packing steps/plots

        This plot is a grid plot, which means that it is result of merging several plots.

        Args:
            render (bool): If True the plot will be rendered else not
            columns (int): the number of columns in which the packing steps are listed
            plot_width (int): the width of the single plot objects
            plot_height (int): the height of the single plot objects

        Returns:
            grid_layout(Column): A plot object with all packing steps inside
        """
        grid = gridplot(self.plot_steps, ncols=columns, plot_width=plot_width, plot_height=plot_height,
                        toolbar_location="left")
        min_border = (int(self.hc.min_border * 10)) / 10
        max_border = (int(self.hc.max_border * 10)) / 10
        title = Div(
            text="<b>Highclass Container&nbsp{},&nbspPolygon&nbspheight:&nbsp{}-{} <b>".format(self.hc.i, min_border,
                                                                                               max_border))
        grid_layout = column(title, grid)
        if render:
            show(grid_layout)
        return grid_layout


class MiniContainer(object):
    """An object which holds the polygons from a container box

    The Container which got packed/build from a HighClass got "theoretically" divided  into boxes. A mini-container is
    an extension of the width of one these boxes.
    The purpose is to have mini-containers with fixed widths so they easily can be stacked, when the
    RectangularContainer is build.

    Args:
        max_width (float): is the maximum width for all mini-containers which will be created
                           formula:= (c+1)* max_width_of_all_polygons_to_pack
        max_height (float): the y boundary of the Container which was divided into the boxes
        max_polygon_width (float): is the value of the max_width_of_all_polygons_to_pack
        hc_i (int): the number of the HighClass starting at 0, hc_0 would be the first HighClass
        c (float): c = 2.214 helps to build the mini-container max_width for more information look into the paper

    Attributes:
        max_polygon_width (float): the value of the max_width_of_all_polygons_to_pack
        current_right_boundary (float): the biggest x coordinate value of all the polygons in the mini-container
        height (float): the biggest y coordinate value of all the polygons in the mini-container
        hc_i (int): the number of the HighClass starting at 0, hc_0 would be the first HighClass
        max_height (float): the y boundary of the Container which was divided into the boxes
        max_width (float): is the maximum width for all mini-containers which will be created
                           formula:= (c+1)* max_width_of_all_polygons_to_pack
        c (float): c = 2.214 helps to build the mini-container max_width for more information look into the paper
        polygons ([ConvexPolygon]): Convex polygons which are in the mini-container
        y_offset (float): offset which helps to stack the mini-container to the  RectangularContainer
    """
    def __init__(self, max_width: float, max_height: float, max_polygon_width: float, hc_i: int, c=2.214) -> None:
        self.max_polygon_width = max_polygon_width
        self.current_right_boundary = 0
        self.height = 0
        self.hc_i = hc_i
        self.max_height = max_height
        self.max_width = max_width
        self.c = c
        self.polygons = []
        self.y_offset = 0

    def plot_container(self, render=True, title="", background_c=None) -> Figure:
        """Creates a plot object of the mini-container

        Args:
            render (bool): if True the mini-container is also rendered else not
            title (str): the title of the plot
            background_c (str): a color for the plot background

        Returns:
            fig (Figure): a plot object of the mini-container
        """
        y_offset = self.y_offset
        legend_polygons = []
        legend_spine = []
        legend_vertices = []
        tooltips = [("index", "$index"), ("(x,y)", "($x{0.0}, $y{0.0})"), ]
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', tooltips=tooltips, toolbar_location="left")
        if background_c is not None:
            color_box = BoxAnnotation(left=0, right=self.max_width, bottom=y_offset,
                                      top=y_offset + self.height, fill_color=background_c,
                                      fill_alpha=0.1)
            fig.add_layout(color_box)
        for counter, polygon in enumerate(self.polygons):
            x_values = polygon.x_values
            y_values = polygon.y_values
            poly_fig = fig.patch(x_values, y_values, line_width=1, alpha=0.1, line_alpha=1, muted_alpha=0.05)
            circle_fig = fig.circle(x_values, y_values, line_width=1, muted_alpha=0.01)
            legend_label = "Polygon{}".format(counter)
            legend_polygons.append((legend_label, [poly_fig]))
            legend_vertices.append(circle_fig)
            x_spine = [x[0] for x in polygon.spine]
            y_spine = [y[1] for y in polygon.spine]
            spine_fig = fig.line(x_spine, y_spine, line_color="red", line_width=1, alpha=0.1, muted_alpha=1,
                                 muted_color="red")
            legend_spine.append(spine_fig)
        # building the custom legend
        fig_boundary = fig.line([0, self.max_width, self.max_width, 0, 0],
                                [0 + y_offset, 0 + y_offset, self.height + y_offset, self.height + y_offset,
                                 0 + y_offset],
                                line_color="black", alpha=0.7, line_width=1, muted_alpha=0.2)
        fig_polygon_boundary = fig.line([self.current_right_boundary, self.current_right_boundary],
                                        [0 + y_offset, self.height + y_offset],
                                        line_color="black", line_dash="dotted", alpha=0.7, line_width=1,
                                        muted_alpha=0.2)
        items = legend_polygons + [("spine", legend_spine)]
        items.append(("container-boundary", [fig_boundary]))
        items.append(("last polygon-boundary", [fig_polygon_boundary]))
        items.append(("vertices", legend_vertices))
        legend = Legend(items=items)
        legend.click_policy = "mute"
        fig.add_layout(legend, 'right')
        if render:
            show(fig)
        return fig


class RectangularContainer(object):
    """The RectangularContainer which holds the packed polygons which are the result of the packing algorithm,

    For building the RectangularContainer all the MiniContainers will be stacked on each other.

    Args:
        mini_container_array ([MiniContainer]): the mini-container to pack
        angle (int): the angle of the RectangularContainer default=0°, will be used for optimizations and to build the
                     convex-container
    Attributes:
        mini_containers ([MiniContainer]): the mini containers in their final position
        initial_mini_containers ([MiniContainer]): a redundant copy of all mini-container in their initial position
        polygons ([ConvexPolygon]): the result of the packing algorithm all start polygons packed
        x_boundary (float): the x boundary of the RectangularContainer
        y_boundary (float): the y boundary of the RectangularContainer
        container_not_clipped_area (float): the area of the RectangularContainer without clipping/optimization,
                                            optimizations are clipping the mini-containers y boundary to the highest
                                            vertex of all polygons inside the MiniContainer instead of using the
                                            HighClass boundary of the MiniContainer.
                                            Another optimization is to clip the RectangularContainer x boundary to
                                            the rightest vertex of all polygons inside the RectangularContainer instead
                                            of using the MiniContainer x boundary.
        polygon_shells ([[Point_xy]]): a list of all packed polygon shells
        x_values_border ([float]): the x values of the RectangularContainer border
        y_values_border ([float]): the y values of the RectangularContainer border
        container_area (float): the optimized/clipped container area
        angle (int): the angle of the RectangularContainer default=0°, will be used for optimizations and to build the
                     convex-container
        plot_steps_all (Tabs): plot object with all steps to pack the RectangularContainer with tabs

    """
    def __init__(self, mini_container_array: [MiniContainer], angle=0) -> None:
        self.mini_containers = copy.deepcopy(mini_container_array)
        self.initial_mini_containers = mini_container_array
        self.polygons, self.x_boundary, self.y_boundary, self.container_not_clipped_area = self.pack_container()
        self.polygon_shells = self.collect_polygon_shells()
        self.x_values_border = [0, 0, self.x_boundary, self.x_boundary, 0]
        self.y_values_border = [0, self.y_boundary, self.y_boundary, 0, 0]
        self.container_area = self.x_boundary * self.y_boundary
        self.angle = angle
        self.plot_steps_all = None

    def collect_polygon_shells(self):
        """Collects the polygon shells of the packed polygons

        Returns:
            polygon_shells ([[Point_xy]]): a list of all packed polygon shells
        """
        polygon_shells = []
        for polygon in self.polygons:
            polygon_shells.append(polygon.shell)
        return polygon_shells

    def pack_container(self) -> ([ConvexPolygon], float, float):
        """Stacks the mini-containers and build the RectangularContainer

        Returns:
            rect_c_polygons ([ConvexPolygon]): packed polygons
            x_boundary (float): x boundary of the RectangularContainer
            y_boundary (float): the y boundary of the RectangularContainer
            container_not_clipped_area (float): area of the not clipped RectangularContainer
        """
        x_boundary = 0
        y_boundary = 0
        rect_c_polygons = []
        container_not_clipped_area = 0
        for mini_container in self.mini_containers:
            container_not_clipped_area += mini_container.max_width * mini_container.max_height
            for polygon in mini_container.polygons:
                polygon.translation(0, y_boundary)
            mini_container.y_offset = y_boundary
            y_boundary += mini_container.height
            rect_c_polygons = rect_c_polygons + mini_container.polygons
            if x_boundary < mini_container.current_right_boundary:
                x_boundary = mini_container.current_right_boundary
        return rect_c_polygons, x_boundary, y_boundary, container_not_clipped_area

    def plot_polygons(self, title="", plot_width=500, plot_height=500, render=True) -> Figure:
        """Creates a plot object for all packed polygons

        Args:
            title (str): title of the plot
            plot_width (int): the width of the plot figure
            plot_height (int): the height of the plot figure
            render (bool): if True the plot object will be rendered else not

        Returns:
            fig (Figure): plot object with all packed polygons
        """
        if title == "":
            title = 'Rectangular-Container  area: {}  not-clipped-area: {}   ' \
                    'angle:{}'.format(truncate(self.container_area, 1), truncate(self.container_not_clipped_area, 1),
                                      self.angle)
        fig = plot_polygons_in_single_plot(self.polygons, title=title, plot_width=plot_width, plot_height=plot_height,
                                           render=render, border=(self.x_values_border, self.y_values_border))
        return fig

    def plot_steps(self, render=True) -> Tabs:
        """Plot object with all steps needed to pack the RectangularContainer

        Args:
            render (bool): if True the plot object will be rendered else not

        Returns:
            plot_steps_all (Tabs): plot object with all steps to pack the RectangularContainer with tabs
        """
        if render:
            show(self.plot_steps_all)
        return self.plot_steps_all

    def plot_container(self, title="", plot_width=600, plot_height=600, solo_box_border=False, render=True,
                       reverse_legend=True) -> Figure:
        """Plot object with all stacked mini-containers with background colors

        Args:
            title (str): title of the plot
            plot_width (int): plot width
            plot_height (int): plot height
            solo_box_border (bool): if True shows for every mini-container an own x boundary which is build by the
                                    rightest polygon in this mini-container
            render (bool): if True the plot object will be rendered else not
            reverse_legend (bool): if True the legend of the plot will be reversed else not

        Returns:
            fig (Figure): plot object with all stacked mini-containers with background colors
        """
        legend_mini_containers = []
        legend_spine = []
        legend_items = []
        legend_vertices = []
        tooltips = [("index", "$index"), ("(x,y)", "($x{0.0}, $y{0.0})"), ]
        if title == "":
            title = 'Rectangular-Container   area: {}   not-clipped-area: {}   angle:{}'.format(
                truncate(self.container_area, 1), truncate(self.container_not_clipped_area, 1), self.angle)
        fig = figure(title=title, x_axis_label='x', y_axis_label='y', width=plot_width, height=plot_height,
                     toolbar_location="left", tooltips=tooltips)
        y_offset = 0
        colors = itertools.cycle(palette)
        for counter, mini_container in enumerate(self.mini_containers):
            color = next(colors)
            legend_polygons = []
            for polygon in mini_container.polygons:
                source = ColumnDataSource(data=dict(x=polygon.x_values, y=polygon.y_values))
                fig_polygon = fig.patch(x="x", y="y", line_width=1, color=color, line_color=color, muted_color=color,
                                        alpha=0.1, muted_alpha=0.05, line_alpha=1, source=source)
                fig_circle = fig.circle(x="x", y="y", line_width=1, color=color, fill_color=color, muted_color=color,
                                        muted_alpha=0.01, size=4,
                                        source=source)
                spine_x_values = [x[0] for x in polygon.spine]
                spine_y_values = [x[1] for x in polygon.spine]
                fig_spine = fig.line(spine_x_values, spine_y_values, line_color="red", line_width=1, alpha=0.01,
                                     muted_color="red",
                                     muted_alpha=1)
                legend_spine.append(fig_spine)
                legend_polygons = legend_polygons + [fig_polygon]
                legend_vertices.append(fig_circle)
            if solo_box_border:
                mini_box_border = mini_container.current_right_boundary
                fig.title.text = 'Rectangular-Container mini-containers with solo boundaries'
            else:
                mini_box_border = self.x_boundary
            color_box = BoxAnnotation(left=0, right=mini_box_border, bottom=mini_container.y_offset,
                                      top=mini_container.y_offset + mini_container.height, fill_color=color,
                                      fill_alpha=0.1)
            fig.add_layout(color_box)
            fig_border = fig.line([0, mini_box_border, mini_box_border, 0, 0],
                                  [0 + y_offset, 0 + y_offset, mini_container.height + y_offset,
                                   mini_container.height + y_offset,
                                   0 + y_offset], line_color=color, muted_alpha=0.2)
            y_offset += mini_container.height
            legend_label = "MC{} height:{} (hc{})".format(counter, int(mini_container.height), mini_container.hc_i)
            legend_polygons.append(fig_border)
            legend_mini_containers.append((legend_label, legend_polygons))
        spine_legend = [("spine", legend_spine)]
        vertices_legend = [("vertices", legend_vertices)]
        legend_items = legend_items + legend_mini_containers + spine_legend + vertices_legend
        if reverse_legend:
            legend_items.reverse()
        legend = Legend(items=legend_items)
        fig.add_layout(legend, 'right')
        fig.legend.click_policy = "mute"
        if render:
            show(fig)
        return fig


class ConvexContainer(object):
    """Convex Container which holds the packed polygons

    To build the convex container the convex input polygons will be rotated with an negative angle = -gamma and packed
    to a RectangularContainer.
    After packing the RectangularContainer its border and all polygons from it will be rotated back with the angle
    positive gamma. These rotation, packing and back rotation steps will be done several times by incrementing the angle
    to find the smallest ConvexContainer. The angle should be between 0 <= |gamma| < 360 to prevent repetition of the
    ConvexContainers. Smaller gamma increments means more repetitions to find the ConvexContainer but the chance to
    skip the smallest ConvexContainer is lower.

    Example:
        gamma=10° increments:=0°,10°,20° ...350° -> 36 times doing the 3 steps rotating ( with -gamma), packing to Rect-
        angularContainer and back rotating (with gamma) of the convex input polygons,
        between the increments could be some angle like 8° where we miss the smallest ConvexContainer.

    Args:
        polygons ([ConvexPolygon]): convex input polygons to pack into the smallest ConvexContainer
        steps (int): the angle for the rotation and back rotation increments
    Attributes:
        angle_0_rectangular_container (ConvexContainer): the input polygons packed to a not rotated RectangularContainer
        angle_0_rectangular_container_polygons ([ConvexPolygon]):  the polygons of the 0° RectangularContainer
        rotate_center (float): the center for rotating the polygons, the center of the 0° RectangularContainer
        rotated_rectangular_container_list ([EndContainer]): the list of all packed RectangularContainers
        rotated_container_plots (Column): plot object which include all packed RectangularContainers
        back_rotated_polygons_plots (Column): plot object which include all back rotated RectangularContainers
        smallest_rectangular_container (EndContainer): smallest RectangularContainer (ConvexContainer)
        polygons ([ConvexPolygon]): the polygon of the smallest RectangularContainer (ConvexContainer)
        angle (int): the angle of the smallest RectangularContainer (ConvexContainer)
        area (float): the area of the smallest RectangularContainer (ConvexContainer)
        polygon_shells ([[Point_xy]]): the shells of the polygons in the smallest RectangularContainer (ConvexContainer)
        width (float): the width of the ConvexContainer
        height (float): the height of the ConvexContainer
        boundary_x_values ([float]): the x values of the ConvexContainer boundary
        boundary_y_values ([float]): the y values of the ConvexContainer boundary
        plot_steps_all (Tabs): all steps of building the smallest RectangularContainer (ConvexContainer) and
                                   including all rotated and back rotated RectangularContainers
    """
    def __init__(self, polygons: [ConvexPolygon], steps=90, build_plots=True) -> None:
        self.angle_0_rectangular_container = pack_polygons(polygons)
        self.angle_0_rectangular_container_polygons = self.angle_0_rectangular_container.polygons
        self.rotate_center = (self.angle_0_rectangular_container.x_boundary / 2,
                              self.angle_0_rectangular_container.y_boundary / 2)
        self.rotated_rectangular_container_list = []  # Liste aller RectangularContainer
        self.rotated_container_plots = []
        self.back_rotated_polygons_plots = []
        self.smallest_rectangular_container, self.polygons, self.angle, self.area = self.find_convex_container(steps, build_plots)
        self.polygon_shells = self.collect_polygon_shells()
        self.width = self.smallest_rectangular_container.x_boundary
        self.height = self.smallest_rectangular_container.y_boundary
        self.boundary_x_values, self.boundary_y_values = self.set_boundaries()
        self.plot_steps_all = self.build_plot_steps()

    def find_convex_container(self, steps: int, build_plots=True) -> (RectangularContainer, [ConvexPolygon], int,
                                                                      float):
        """Finds the ConvexContainer and collects the steps as plot objects

        To find the convex container the convex input polygons will be rotated with an negative angle=-gamma and packed
        to a RectangularContainer.
        After packing the RectangularContainer its border and all polygons from it will be rotated back with the angle
        positive gamma. The RectangularContainer with the smallest area is the ConvexContainer result.

        Args:
            steps (int): the increment steps (degree) for rotating the polygons
            build_plots (bool): if True all the RectangularContainer plot steps which will be done to find the Convex-
                                Container will be collected that means even the RectangularContainers which are not the
                                smallest will be back rotated and a plot object will created
                                -> more redundant steps and worse performance but winning more information
                                else no plot objects for the steps are build ->  just the end ConvexContainer and his
                                polygons can be plotted
        Returns:
            smallest_container (RectangularContainer): the smallest RectangularContainer  not back rotated
            smallest_back_rotated_polygons: the polygons of the smallest RectangularContainer back rotated which are
                                            building the ConvexContainer by adding the boundary
            abs_angle (int): the rotation angle of the smallest RectangularContainer (ConvexContainer)
            smallest_area (float): the area of the smallest RectangularContainer (ConvexContainer)
        """
        list_of_area = []
        for angle in range(0, 360, steps):
            polygons = rotate_polygons(self.angle_0_rectangular_container_polygons, -angle, origin=self.rotate_center)
            rotated_rectangular_container = pack_polygons(polygons, -angle)
            self.rotated_rectangular_container_list.append(rotated_rectangular_container)
            list_of_area.append(rotated_rectangular_container)
            if build_plots:
                title = 'Rectangular-Container  area: {}  not-clipped-area: {} angle: {}'.format(
                    truncate(rotated_rectangular_container.container_area, 1),
                    truncate(rotated_rectangular_container.container_not_clipped_area, 1),
                    rotated_rectangular_container.angle)
                self.rotated_container_plots.append(rotated_rectangular_container.plot_container(render=False,
                                                                                                 title=title))
                back_rotated_polygons = rotate_polygons(rotated_rectangular_container.polygons, angle,
                                                        origin=self.rotate_center)
                box_x_v = rotated_rectangular_container.x_values_border
                box_y_v = rotated_rectangular_container.y_values_border
                boundaries = list(zip(box_x_v, box_y_v))
                rotated_x_values, rotated_y_values = rotate_points_and_split(boundaries, angle,
                                                                             origin=self.rotate_center)
                title2 = 'Convex-Container  area: {}  not-clipped-area: {} angle: {}'.format(
                    truncate(rotated_rectangular_container.container_area, 1),
                    truncate(rotated_rectangular_container.container_not_clipped_area, 1),
                    -rotated_rectangular_container.angle)
                back_rotated_polygon_plot = plot_polygons_in_single_plot(back_rotated_polygons, render=False,
                                                                         border=(rotated_x_values, rotated_y_values),
                                                                         title=title2)
                self.back_rotated_polygons_plots.append(back_rotated_polygon_plot)
        sorted_container = sorted(list_of_area, key=lambda k: k.container_area)
        smallest_container = sorted_container[0]
        angle = smallest_container.angle
        smallest_container_polygons = sorted_container[0].polygons
        smallest_back_rotated_polygons = rotate_polygons(smallest_container_polygons, angle, origin=self.rotate_center)
        smallest_area = smallest_container.container_area
        abs_angle = abs(angle)
        return smallest_container, smallest_back_rotated_polygons, abs_angle, smallest_area

    def collect_polygon_shells(self) -> [[Point_xy]]:
        """Collects the polygon shells of all packed polygons in the ConvexContainer

        Returns:
            polygon_shells ([[Point_xy]]): polygon shells of all packed polygons in the ConvexContainer
        """
        polygon_shells = []
        for polygon in self.polygons:
            polygon_shells.append(polygon.shell)
        return polygon_shells

    def set_boundaries(self) -> ([Point_xy], [Point_xy]):
        """Sets the boundaries for the ConvexContainer

        Returns:
            rotated_x_values, rotated_y_values ([Point_xy], [Point_xy]): x and y values of the boundary in a tuple
        """
        container = self.smallest_rectangular_container
        boundary_x_values = [0, 0, self.width, self.width, 0]
        boundary_y_values = [0, self.height, self.height, 0, 0]
        boundaries = list(zip(boundary_x_values, boundary_y_values))
        rotated_x_values, rotated_y_values = rotate_points_and_split(boundaries, container.angle,
                                                                     origin=self.rotate_center)
        return rotated_x_values, rotated_y_values

    def build_plot_steps(self) -> Tabs:
        """
        Build the plot object with all steps to build the smallest RectangularContainer and all rotated Rectangular-
        Containers and back rotated polygons

        Returns:
            tab_header (Tabs): a plot object which is organized with tabs
        """
        convex_c_panels = []
        smallest_c = self.plot_container(render=False, plot_width=800, plot_height=700)
        smallest_c_tab = Panel(child=smallest_c, title="Smallest-Convex-Container")
        convex_c_panels.append(smallest_c_tab)

        rotated_c = self.plot_rotated_containers(render=False, plot_width=800, plot_height=700)
        rotated_c_tab = Panel(child=rotated_c, title="Rotated-Rectangular-Containers")
        convex_c_panels.append(rotated_c_tab)

        back_rotated_c = self.plot_back_rotated_polygons(render=False, plot_width=800, plot_height=700)
        back_rotated_c_tab = Panel(child=back_rotated_c, title="Convex-Containers (back rotated)")
        convex_c_panels.append(back_rotated_c_tab)

        tabs = Tabs(tabs=convex_c_panels)
        t_header = Panel(child=tabs, title="Convex-Container")  #
        tab_header = self.smallest_rectangular_container.plot_steps(render=False)
        tab_header.tabs.append(t_header)
        return tab_header

    def plot_steps(self, render=True) -> Tabs:
        """Plot including the ConvexContainer, rotated RectangularContainers and back rotated polygons

        Args:
            render (bool): if True render the plot else not

        Returns:
            plot_steps_all (Tabs): a plot object which is organized with tabs
        """
        if render:
            show(self.plot_steps_all)
        return self.plot_steps_all

    def plot_rotated_containers(self, render=True, columns=9, plot_width=600, plot_height=600) -> Row:
        """Plot including all rotated RectangularContainers

        Args:
            render (bool): if True render the plot else not
            columns (int): the column count in which the container plots will be rendered
            plot_width (int): the width of the plot figure
            plot_height (int): the height of the plot figure

        Returns:
            grid (Row): grid plot including all rotated RectangularContainers
        """
        grid = gridplot(self.rotated_container_plots, ncols=columns, plot_width=plot_width, plot_height=plot_height,
                        toolbar_location="left")
        if render:
            show(grid)
        return grid

    def plot_back_rotated_polygons(self, render=True, columns=9, plot_width=700, plot_height=600) -> Row:
        """Plot including all back rotated polygons of the RectangularContainers

        Args:
            render (bool): if True render the plot else not
            columns (int): the column count in which the container plots will be rendered
            plot_width (int): the width of the plot figure
            plot_height (int): the height of the plot figure

        Returns:
            grid (Row): grid plot including all back rotated polygons of the RectangularContainers
        """
        grid = gridplot(self.back_rotated_polygons_plots, ncols=columns, plot_width=plot_width, plot_height=plot_height,
                        toolbar_location="left")
        if render:
            show(grid)
        return grid

    def plot_container(self, title="", plot_width=600, plot_height=600, render=True) -> Figure:
        """Plot of the ConvexContainer

        Args:
            title (str): the title of the ConvexContainer
            plot_width (int): width of the plot figure
            plot_height (int): height of the plot figure
            render (bol): if True the plot will be rendered else not

        Returns:
            fig (Figure): ConvexContainer plot object
        """
        if title == "":
            title = 'Convex Container  area: {}  not-clipped-area: {}  angle: {}'.format(
                truncate(self.smallest_rectangular_container.container_area, 1),
                truncate(self.smallest_rectangular_container.container_not_clipped_area, 1), self.angle)
        fig = plot_polygons_in_single_plot(self.polygons, title=title, plot_width=plot_width, plot_height=plot_height,
                                           render=render, border=(self.boundary_x_values, self.boundary_y_values))
        return fig


def create_multiple_convex_polygons(number: int, max_ngon: int, max_rand=1000) -> [ConvexPolygon]:
    """Creates multiple convex polygons

    Args:
        number (int): the number of the polygons to create
        max_ngon (int): the maximum vertices a polygon can have
        max_rand (int): the maximum value for the x and y coordinate of the polygon vertices

    Returns:
        polygon_list ([ConvexPolygon]): a list of convex polygons
    """
    polygon_list = []
    for count in range(0, number):
        polygon_list.append(create_convex_polygon(max_ngon, max_rand))
    return polygon_list


def create_convex_polygon(max_ngon: int, max_rand=1000) -> ConvexPolygon:
    """Creates a convex Polygon

    Uses the convex hull function from the shapely package.
    The way the polygon is created is to create max_ngon points and then use the convex hull on these points which means
    that the output polygon does not have allways max_ngon vertices.

    Args:
        max_ngon (int): the maximum vertices a polygon can have
        max_rand (int): the maximum value for the x and y coordinate of the polygon vertices

    Returns:
        c_polygon (ConvexPolygon):
    """
    ngon = numpy.random.randint(3, max_ngon + 1)
    convex_polygon = None
    while type(convex_polygon) is not Polygon:  # the shapely convex hull could create a point or stretch
        polygon_points = []
        while len(polygon_points) < ngon:
            x = numpy.random.randint(0, max_rand + 1)
            y = numpy.random.randint(0, max_rand + 1)
            while (x, y) in polygon_points:
                x = numpy.random.randint(0, max_rand + 1)
                y = numpy.random.randint(0, max_rand + 1)
            polygon_points.append((x, y))
        convex_polygon = Polygon(polygon_points).convex_hull
    c_polygon = ConvexPolygon(list(convex_polygon.exterior.coords))
    return c_polygon


def build_height_classes(polygon_list: [ConvexPolygon]) -> [HighClass]:
    """Creates HighClasses and assigns the input polygons to them

    Args:
        polygon_list ([ConvexPolygons]): list of the convex input polygons

    Returns:
        height_classes ([HeightClass]): a list of created height classes
    """
    ordered_polygons = sorted(polygon_list, key=lambda polygon: polygon.height, reverse=True)
    h_max_polygon = ordered_polygons[0].height
    w_max_polygon = max([polygon.width for polygon in polygon_list])
    alpha = 0.407
    i = 0
    height_classes = []
    polygon_count = len(ordered_polygons)
    while polygon_count > 0:
        hc = HighClass(i, alpha, h_max_polygon, w_max_polygon)
        hc_polygons = []
        while polygon_count > 0 and hc.min_border < ordered_polygons[0].height <= hc.max_border:
            hc_polygons.append(ordered_polygons.pop(0))
            polygon_count -= 1
        hc.set_polygons(hc_polygons)
        if len(hc.polygons) > 0:
            height_classes.append(hc)
        i += 1
    return height_classes


def build_containers(height_classes: [HighClass]) -> [Container]:
    """Wrapper function for building a Container for each HighClass

    Args:
        height_classes ([HeighClass]): list of input high classes

    Returns:
        container ([Container]): a list of build containers
    """
    containers = []
    for height_class in height_classes:
        container = Container(height_class)
        containers.append(container)
    return containers


def build_mini_containers_and_plots(container_array: [Container], c=2.214) -> ([MiniContainer], [[Figure]]):
    """Divides a Container into boxes and creates mini-containers by extending these

    This function also creates plot objects for every important step to build the mini-containers.

    Args:
        container_array ([Container]): a list of all container every container is build from a highclass
        c (float): the values which decides the width of the mini-containers for more information look into the paper

    Returns:
        mini_container_array, mini_container_plots_list ([MiniContainer], [[Figure]]): a tuple of all build
                                                                                       mini-containers and all important
                                                                                       plot steps, every list of plots
                                                                                       represent a Container and the
                                                                                       resulting mini-containers
    """
    mini_container_array = []
    mini_container_plots_list = []
    colors = itertools.cycle(palette)
    background_color_list = [next(colors)]
    for container in container_array:
        container_sigma = copy.deepcopy(container.sigma)
        container_and_mini_container_plots = []
        box_width = c * container.hc.w_max_polygon
        box_width_helper = 0
        box_counter = 1  # helper for dividing the container into boxes
        box_boundaries_x_values = []
        # finding the box boundaries for plot
        while box_width_helper < container.x_boundary:
            box_boundaries_x_values.append(box_width_helper)
            box_width_helper += box_width
            background_color_list.append(next(colors))
        box_boundaries_x_values_colors_tuple = (box_boundaries_x_values, background_color_list)
        container_and_mini_container_plots.append(container.plot_container(container_sigma, render=False,
                                                                           box_boundaries_x_values_colors_tuple
                                                                           =box_boundaries_x_values_colors_tuple))
        max_width_mini_container = box_width + container.hc.w_max_polygon
        background_counter = 0
        while len(container_sigma) > 0:
            mini_container = MiniContainer(max_width_mini_container, container.y_boundary,
                                           container.hc.w_max_polygon, container.hc.i)
            min_translation = math.inf
            mini_container_y_border = 0
            mini_container_x_border = 0
            pop_list = []  # holds the indices of polygons which are assigned to the box
            for counter, polygon in enumerate(container_sigma):
                if polygon.min_x < (box_width * box_counter):
                    pop_list.append(counter)
                    if polygon.min_x < min_translation:
                        min_translation = polygon.min_x
            pop_helper = 0
            # translating the polygons assigned the the box and adding them to the mini container
            if pop_list:
                for counter in pop_list:
                    container_sigma[counter - pop_helper].translation(-min_translation, 0)
                    if mini_container_y_border < container_sigma[counter - pop_helper].max_y:
                        mini_container_y_border = container_sigma[counter - pop_helper].max_y
                    if mini_container_x_border < container_sigma[counter - pop_helper].max_x:
                        mini_container_x_border = container_sigma[counter - pop_helper].max_x
                    mini_container.polygons.append(container_sigma.pop(counter - pop_helper))
                    pop_helper += 1
            mini_container.height = mini_container_y_border
            mini_container.current_right_boundary = mini_container_x_border
            mini_container_array.append(mini_container)
            if (box_width * box_counter) < container.x_boundary:
                title = "Mini-Container{}  height: {}  (hc_{})".format(box_counter, truncate(mini_container.height, 1),
                                                                       container.hc.i)
                b_color = background_color_list[background_counter]
                container_and_mini_container_plots.append(
                    mini_container.plot_container(title=title, render=False, background_c=b_color))
                box_counter += 1
                background_counter += 1
            # for the last box which can be smaller
            else:
                title = "Mini-Container{}  height: {}  (hc_{})".format(box_counter, truncate(mini_container.height, 1),
                                                                       container.hc.i)
                container_and_mini_container_plots.append(mini_container.plot_container(title=title, render=False))
        mini_container_plots_list.append(container_and_mini_container_plots)
    return mini_container_array, mini_container_plots_list


def pack_polygons(polygons: [ConvexPolygon], angle=0) -> RectangularContainer:
    """Wrapper for the packing algorithm, packs the  convex input polygons into a RectangularContainer

    Packing Steps:
        0. Condition that the input polygons are convex
        1. Sort the polygons into HighClasses
        2. For every HighClass build a Container with fixed height
        3. Divide every Container into MiniContainers with fixed width
            3.1 optimization: clip the mini-container height to the highest vertex point of the mini-container
        4. Stack the MiniContainer to a RectangularContainer
            4.1 optimization: clip the RectangularContainer width to the rightest polygon vertex of all polygons in the
                RectangularContainer

    The pack_polygons function also bundles all important packing steps in one plot.

    Args:
        polygons ([ConvexPolygon]): convex input polygon which should packed
        angle (int): the angle of the Rectangular container default 0, this argument helps for finding the
                     ConvexContainer

    Returns:
        rectangular_container (RectangularContainer): the packed polygons in a RectangularContainer, the packing steps
                                                      are saved in this object
    """
    # building the RectangularContainer
    list_hc = build_height_classes(polygons)
    list_containers = build_containers(list_hc)
    list_mini_containers, mini_container_plot_steps = build_mini_containers_and_plots(list_containers)
    rectangular_container = RectangularContainer(list_mini_containers, angle)

    # plots
    p_tab = polygons_to_tab_plot(polygons)
    p_header_tab = Panel(child=p_tab, title="Polygons")
    hc_tab = hc_to_tab_plot(list_hc)
    hc_header_tab = Panel(child=hc_tab, title="Height-Classes")
    c_tab = containers_to_tab_plot(list_containers)
    c_header_tab = Panel(child=c_tab, title="Containers")
    mc_tab = mini_container_plots_to_tab_plot(mini_container_plot_steps)
    mc_header_tab = Panel(child=mc_tab, title="Mini-Containers")
    ec_tab = rectangular_container_to_tab_plot(rectangular_container)
    ec_header_tab = Panel(child=ec_tab, title="Rectangular-Container")
    all_tabs_with_header = Tabs(tabs=[p_header_tab, hc_header_tab, c_header_tab, mc_header_tab, ec_header_tab])

    rectangular_container.plot_steps_all = all_tabs_with_header
    return rectangular_container


def rotate_points_and_split(point_list: [Point_xy], angle, origin=(0, 0)) -> ([float], [float]):
    """Rotates the input points and give a tuple with x and y values back

    Uses the affinity.rotated function from the shapely package which creates a dependency, another way would be to use
    a own function (rotation matrix).

    Hint:
        when writing an own function to rotate the points, their are some special cases in python which need to be
        handled,

        this is a code snipped from the shapely package to show these cases
        code snipped from: https://github.com/Toblerity/Shapely/blob/master/shapely/affinity.py
        angle = angle * pi/180.0
        cosp = cos(angle)
        sinp = sin(angle)
        if abs(cosp) < 2.5e-16:  # if not checked this case could create confusion
            cosp = 0.0
        if abs(sinp) < 2.5e-16:  # if not checked this case could create confusion
            sinp = 0.0

    Args:
        point_list ([Point_xy]): a list of points which need to be rotated
        angle (int): the rotation angle
        origin ((float,float)): the rotation point for all rotations

    Returns:
        poly_wrapper_rotated.exterior.xy (([float], [float])): tuple with rotated x and y values
    """
    # wrapping the points into a shapely Polygon to use the rotation function
    poly_wrapper = Polygon(point_list)
    poly_wrapper_rotated = affinity.rotate(poly_wrapper, angle, origin=origin)
    return poly_wrapper_rotated.exterior.xy


def rotate_and_create_new_convex_polygon(polygon: ConvexPolygon, angle: int, origin=(0, 0)) -> ConvexPolygon:
    """Creates a new rotated convex polygon from an input convex polygon

    This function uses again the affinity.rotate() function from shapely.
    This creates a dependency to the shapely package.

    Args:
        polygon (ConvexPolygon): convex polygon which will "copied" and rotated
        angle (int): the rotation angle
        origin ((float,float)): the rotation point for all vertex rotations

    Returns:
        rotated_convex_polygon (ConvexPolygon): rotated convex polygon
    """
    poly_wrapper = Polygon(polygon.shell)
    poly_wrapper_rotated = affinity.rotate(poly_wrapper, angle, origin=origin)
    rotated_convex_polygon = ConvexPolygon(poly_wrapper_rotated.exterior.coords)
    return rotated_convex_polygon


def rotate_polygons(polygons: [ConvexPolygon], angle: int, origin=(0, 0)) -> [ConvexPolygon]:
    """Creates several new rotated convex polygons

    Args:
        polygons ([ConvexPolygon]): convex polygons which will "copied" and rotated
        angle (int): the rotation angle
        origin ((float,float)): the rotation point for all vertex rotations

    Returns:
        rotated_polygons ([ConvexPolygon]): rotated convex polygons
    """
    rotated_polygons = []
    for polygon in polygons:
        rotated_polygon = rotate_and_create_new_convex_polygon(polygon, angle, origin)
        rotated_polygons.append(rotated_polygon)
    return rotated_polygons


def truncate(number: float, decimals=0) -> float:
    """Returns a value truncated to a specific number of decimal places.

    Source of the this function code is from: https://kodify.net/python/math/truncate-decimals/
    References:
    Python.org (n.d.). math — Mathematical functions. Retrieved on October 22, 2019,from https://docs.python.org/3.8/library/math.html
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)
    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor


def plot_polygons(polygon_list: [ConvexPolygon], render=True, plot_width=450, plot_height=450, columns=4) -> Column:
    """Build a plot object for a list of convex polygons

    Args:
        polygon_list ([ConvexPolygon]): list of convex polygons
        render (bool): if True the plot will be render else not
        plot_width (int): the width of the plot figure
        plot_height (int): the height of the plot figure
        columns (int): the column count in which polygons of the plot are shown

    Returns:
        grid (Column): a plot of all input polygons
    """
    plots = []
    colors = itertools.cycle(palette)
    for counter, polygon in enumerate(polygon_list):
        color = next(colors)
        title = "Polygon {}".format(counter)
        plots.append(polygon.plot_polygon(title=title, color=color, render=False))
    grid = gridplot(plots, ncols=columns, plot_width=plot_width, plot_height=plot_height, toolbar_location="left")
    if render:
        show(grid)
    return grid


def plot_polygons_in_single_plot(polygon_list: [ConvexPolygon], title="", plot_width=750, plot_height=500, render=True,
                                 border=None, reverse_legend=False) -> Figure:
    """Creates a plot object for  several polygons in one coordinate system

    Args:
        polygon_list ([ConvexPolygon]): a list of convex polygons
        title (str): the title of the plot
        plot_width (int): the width of the plot figure
        plot_height (int): the height of the plot figure
        render (bool): if True the plot will be rendered else not
        border (([float],[float])): a tuple of x and y values to create border for the polygons
        reverse_legend (bool): if True the plot legend will be reversed

    Returns:
        fig (Figure): a plot object which shows all input polygons in one coordinate system
    """

    tooltips = [("index", "$index"), ("(x,y)", "($x{0.0}, $y{0.0})"), ]
    fig = figure(title=title, x_axis_label='x', y_axis_label='y', width=plot_width, height=plot_height,
                 tooltips=tooltips, toolbar_location="left")
    colors = itertools.cycle(palette)
    legend_items = []
    legend_polygons = []
    legend_all_polygons = []
    legend_vertices = []
    for counter, polygon in enumerate(polygon_list):
        color = next(colors)
        x = polygon.x_values
        y = polygon.y_values
        if polygon.plot_fill:
            legend_label = "polygon {} filled".format(counter)
            poly_fig = fig.patch(x, y, color=color, line_width=1, alpha=0.9, line_alpha=1, muted_alpha=0.05)
        else:
            legend_label = "polygon {}".format(counter)
            poly_fig = fig.patch(x, y, color=color, line_width=1, alpha=0.1, line_alpha=1, muted_alpha=0.05)
        circle_fig = fig.circle(x, y, color=color, line_width=1, muted_alpha=0.01, size=4)
        legend_polygons.append((legend_label, [poly_fig]))
        legend_vertices.append(circle_fig)
        legend_all_polygons = legend_all_polygons + [poly_fig]
    if border is not None:
        fig_border = fig.line(border[0], border[1], line_color="red", line_width=1, muted_alpha=0.2)
        legend_items.append(("border", [fig_border]))

    legend_items.append(("vertices", legend_vertices))
    legend_items.append(("all polygons", legend_all_polygons))
    legend_items = legend_items + legend_polygons
    if reverse_legend:
        legend_items.reverse()
    legend = Legend(items=legend_items)
    legend.label_text_font_size = "10px"
    legend.click_policy = "mute"
    fig.add_layout(legend, 'right')
    if render:
        show(fig)
    return fig


def polygons_to_tab_plot(polygons: [ConvexPolygon], tab_poly_count=9, columns=3, plot_width=450, plot_height=450) -> \
                        Tabs:
    """Plot of convex input polygons structured with tabs

    Args:
        polygons ([ConvexPolyogn]): convex input Polygons
        tab_poly_count (int): number of polygons in one tab
        columns (int): the columns to present the polygons
        plot_width (int): the width of the plot figure
        plot_height (int): the width of the plot figure

    Returns:
        polygon_tabs (Tabs): a plot object which represents all convex input polygons structured with tabs
    """
    polygon_tab_list = []
    polygons_in_one_tab = []
    colors = itertools.cycle(palette)
    for counter, polygon in enumerate(polygons):
        color = next(colors)
        polygon_title = "Polygon{}".format(counter)
        polygon_plot = polygon.plot_polygon(title=polygon_title, color=color, render=False)
        polygons_in_one_tab.append(polygon_plot)
        if len(polygons_in_one_tab) >= tab_poly_count or (counter + 1 == len(polygons)):
            if counter + 1 - tab_poly_count < 0:
                title = "Polygons {}-{}".format(0, counter)
            else:
                title = "Polygons {}-{}".format(counter + 1 - len(polygons_in_one_tab), counter)
            grid_title = Div(text="<b>{}<b>".format(title))
            polygons_grid = gridplot(polygons_in_one_tab, ncols=columns, plot_width=plot_width, plot_height=plot_height,
                                     toolbar_location="left")
            tab_layout = layout(grid_title, polygons_grid)
            tab = Panel(child=tab_layout, title=title)
            polygon_tab_list.append(tab)
            polygons_in_one_tab = []
    polygon_tabs = Tabs(tabs=polygon_tab_list)
    return polygon_tabs


def hc_to_tab_plot(list_hc: [HighClass], plot_width=450, plot_height=450) -> Tabs:
    """Plot of the input HighClasses structured with tabs

    Args:
        list_hc ([HighClass]): list of the input HighClasses
        plot_width (int): width of the plot figure
        plot_height (int): height of the plot figure

    Returns:
       hc_tabs (Tabs): a plot object which represents the input HighClasses structured with tabs
    """
    hc_tab_list = []
    for counter, hc in enumerate(list_hc):
        hc_plot = hc.plot_hc(render=False, plot_width=plot_width, plot_height=plot_height)
        tab = Panel(child=hc_plot, title="High-Class {}".format(hc.i))
        hc_tab_list.append(tab)
    hc_tabs = Tabs(tabs=hc_tab_list)
    return hc_tabs


def containers_to_tab_plot(list_containers: [Container], plot_width=700, plot_height=700, columns=3) -> Tabs:
    """Plot of the input Containers structured with tabs

    Args:
        list_containers (): list of the input Containers
        plot_width (int): width of the plot figure
        plot_height (int): height of the plot figure
        columns (int): the columns to present the polygons

    Returns:
        container_tabs (Tabs): a plot object which represents the input Containers structured with tabs
    """
    container_tab_list = []
    for counter, container in enumerate(list_containers):
        container_plot = container.plot_container_steps(plot_width=plot_width, columns=columns, plot_height=plot_height,
                                                        render=False)
        tab = Panel(child=container_plot, title="Hc_{} Container".format(container.hc.i))
        container_tab_list.append(tab)
    container_tabs = Tabs(tabs=container_tab_list)
    return container_tabs


def mini_container_plots_to_tab_plot(mini_container_plot_steps: [[Figure]], plot_width=700, plot_height=700, columns=3)\
        -> Tabs:
    """Plot of the input mini-container plot object structured with tabs

    Args:
        mini_container_plot_steps ([[Figure]]): a list of plot objects lists, every list includes all steps from packing
                                                a container to mini-containers
        plot_width (int): the width of the plot figure
        plot_height (int): the height of the plot figure
        columns (int): the columns to present the polygons

    Returns:
        container_tabs (Tabs): a plot object which represents the MiniContainers structured with tabs
    """
    mini_plots_tabs = []
    for counter, m_plot in enumerate(mini_container_plot_steps):
        title = m_plot[0].title.text
        m_plot_grid = gridplot(m_plot, ncols=columns, plot_width=plot_width, plot_height=plot_height,
                               toolbar_location="left")
        grid_title = Div(text="<b>{}<b>".format(title))
        mc_layout = layout(grid_title, m_plot_grid)
        tab = Panel(child=mc_layout, title=title)
        mini_plots_tabs.append(tab)
    mini_tabs = Tabs(tabs=mini_plots_tabs)
    return mini_tabs


def rectangular_container_to_tab_plot(rectangular_container: RectangularContainer, plot_width=800, plot_height=800)\
        -> Tabs:
    """Plot of the input RectangularContainer structured with tabs, one tab shows the mini-containers and the other hide

    Args:
        rectangular_container (RectangularContainer):
        plot_width (int): the width of the plot figure
        plot_height (int): the height of the plot figure

    Returns:
        end_tabs (Tabs) -> a plot object which represents the RectangularContainer structured with tabs
    """
    rectangular_container_tabs = []
    polygons_plot = rectangular_container.plot_polygons(render=False, plot_width=plot_width, plot_height=plot_height)
    tab = Panel(child=polygons_plot, title="Rectangular-Container")
    rectangular_container_tabs.append(tab)
    container_plot = rectangular_container.plot_container(render=False, plot_width=plot_width, plot_height=plot_height)
    tab = Panel(child=container_plot, title="Show Mini-Containers")
    rectangular_container_tabs.append(tab)
    end_tabs = Tabs(tabs=rectangular_container_tabs)
    return end_tabs


def plot_figures_as_grid(plot_list: [Figure], render=True, plot_width=600, plot_height=500, columns=4) -> Column:
    """Creates a grid plot for multiple plot objects

    Args:
        plot_list ([Figure]): list of plot objects
        render (bool): if True the build plot object will be rendered else not
        plot_width (int): the width of the for the plot objects
        plot_height (height): the height of the plot objects
        columns (int): shows the plot objects in columns

    Returns:

    """
    grid = gridplot(plot_list, ncols=columns, plot_width=plot_width, plot_height=plot_height, toolbar_location="left")
    if render:
        show(grid)
    return grid
