from django.urls import path
from plots.views import PolygonEditView, PackingRectContainer, PackingRotatedRectContainer, PackingConvexContainer, \
    CSVPolygons, ClearPolygons, CreateConvexPolygons


urlpatterns = [
    path('', PolygonEditView.as_view(), name='start-page'),
    path('packing_rect_container/', PackingRectContainer.as_view(), name='rect_container'),
    path('packing_rotated_rect_container/', PackingRotatedRectContainer.as_view(), name='rotated_rect_container'),
    path('packing_convex_container/', PackingConvexContainer.as_view(), name='convex_container'),
    path('csv_polygons/', CSVPolygons.as_view(), name='csv_polygons'),
    path('clear_polygons/', ClearPolygons.as_view(), name='csv_polygons'),
    path('create_convex_polygons/', CreateConvexPolygons.as_view(), name='create_convex_polygons'),
]
