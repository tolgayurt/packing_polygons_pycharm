# Packing_Polygons_Pycharm

Configuration:  
-install python 3.8.6 (https://www.python.org/downloads/release/python-386/), while installation menu klick at the box add python to the path  
-install git: (https://git-scm.com/download/win)

0. open a new terminal (its important to open a new terminal after the python and git installation) and navigate to a directory where to install the repository
1. clone the project, write in the terminal:   git clone https://git.imp.fu-berlin.de/tolgayurt/packing_polygons_pycharm.git
2. navigate to the repository packing_polygons_pycharm\mysite
3. install a new venv with the python version 3.8 in the mysite directory  ...\mysite> with:   py -3.8 -m venv myvenv  
(3.1) if step 3. didn't worked install a venv without a special python version with: python -m venv myvenv
4. activate the venv with:   myvenv\Scripts\activate   
 (if (myvenv) is written infront of the command line it is activeted)
5. update pip with:   python -m pip install -U pip
6. install packages from requirements\base.txt: pip install -r requirements\base.txt
7. downgrade the numpy package because of a bug:   pip install numpy==1.19.3     
(error information https://stackoverflow.com/questions/64654805/how-do-you-fix-runtimeerror-package-fails-to-pass-a-sanity-check-for-numpy-an )
Usage:  
8. start the server with: python manage.py runserver  
9. open your webbrowser (internet explorer could not support every function so open mozilla, chrome or opera ...) and write the url: http://127.0.0.1:8000/  
10. if needed the local running server can be quit with ctrl+c

**Usage of the server after all steps are done first time:**    
For several usage in example when the terminal is closed, or computer is shut down.  
Not all steps need to be done again, just activate the venv with the name myvenv (if not allready acitvated, dont forget to navigate to the directory packing_polygons_pycharm\mysite) like in step 4 and do step 8 and 9 again.    
For deactivating the venv if needed navigate to packing_polygons\mysite and write: myvenv\Scripts\deactivate

---------------------------------------------------------------------------------
Linux
1) git clone https://git.imp.fu-berlin.de/tolgayurt/packing_polygons_pycharm.git
2) ./packing_polygons_pycharm/mysite/venv/Scripts/activate
3) pip install -r packing_polygons_pycharm/mysite/requirements/base.txt
4) python packing_polygons_pycharm/mysite/manage.py runserver



Start your browser to use the Server and navigate to the url http://127.0.0.1:8000/

---------------------------------------------------------------------------------
Working Concept.

1. Create Data for the drawtool  
1.1 draw or edit polygons with the buttons in top right legend
- in the PolyDrawTool tab on (https://docs.bokeh.org/en/latest/docs/user_guide/tools.html#edit-tools) is animated guide
- in the PolyEditTool tab on the same site are explination for adding or deleting vertices
- in some browser the delete button is not working for the draw tool because the website is accepting the input, you need to turn it off 
- to delete vertices  
1.2 Below the Drawtool is also an polygon creator button which can create polygons.  
1.3 Or write your Polygons into the polygon.txt file, path: packing_polygons_pycharm/mysite/plots/polygon.txt,an example how to write the polygons to polygons.txt file is already included every line is representing one polygon  

2. The polygons which got created need to be loaded into the storage  
2.1 With the button load all polygon which are visible in the drawtool they will be loaded into the storage  
2.2 With the clear button all loaded polygons will be deleted from storage  

3. Choose one of the packing variants to pack the stored polygons
    - axis-aligned rectangular container (without rotation optimization)
    - axis-aligned rectangular container with 0, 90, 180, 270 rotation optimization
    - rectangular container ("convex container")


----------------------------------------------------------------------------
**Relevant Files (main logic of the software)**  
-The implemented polygon aglortihm is in the file packing_algo.py (mysite/plots/packing_algo.py)  
-The AVLTree structure is in the avl_tree.py (packing_polygons_pycharm/mysite/plots/avl_tree.py)  
-The data_creator algorithm for cutting a rectangle into convex polygons are in (packing_polygons_pycharm/mysite/plots/polygon_creator.py)   
-The analyze data are in the path (packing_polygons_pycharm/mysite/result_of_the_analyzes)
